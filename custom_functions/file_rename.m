function [] = file_rename()

%cd(path)
files = dir('*.bmp');

numFiles = length(files);

for i = 1: numFiles
    temp = files(i).name;
    movefile(temp, sprintf('%05d.bmp', i));

end
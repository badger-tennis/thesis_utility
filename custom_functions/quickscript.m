files = exrInfo(checkEXR);
tic
%tot = length(files);
imsizes = zeros(length(files),3);
aspect = zeros(length(files),1);
originals = cell(length(files),1);
tall = 0;
normal = 0;
normalbool = zeros(length(files),1);
long = 0;
longbool = zeros(length(files),1);
ideal = 0;

for i = 1: length(files)
    im = exrread(files(i).name);
    originals{i} = im;
    imsizes(i,:) = size(im);
    aspect(i) = imsizes(i,2)/imsizes(i,1);
    if 1.7 < aspect(i) && aspect(i) <= (1920/1080)
        ideal = ideal + 1;
    end
    if imsizes(i, 1) > imsizes(i,2)
        tall = tall + 1;
    elseif (aspect(i) > (1.8))
        long = long + 1;
        longbool(i) = 1;
        normalbool(i) = 1;
        normal = normal + 1;
    else
        normal = normal + 1;
        normalbool(i) = 1;
    end
end
toc
reg_ims = cell(normal, 1);
names = cell(normal, 1);
reg_im_count = 1;
reg_sizes = zeros(normal, 3);
reg_aspects = zeros(normal,1);
reg_long = zeros(normal, 1);
for i = 1: length(files)
    if normalbool(i) == 1
        reg_ims{reg_im_count} = originals{i};
        names{reg_im_count} = files(i).name;
        reg_sizes(reg_im_count, :) = size(reg_ims{reg_im_count});
        reg_aspects(reg_im_count) = aspect(i);
        if longbool(i) == 1
            reg_long(reg_im_count) = 1;
        end
        reg_im_count = reg_im_count + 1;
        
    end
    clear originals{i};
end

clear originals;
resized = cell(normal, 1);
res_dim = zeros(normal, 3);
bool1080 = zeros(normal, 1);
for i = 1 : normal
    if reg_long(i) == 1
        resized{i,1} = imresize(reg_ims{i}, [1080, NaN], 'bilinear');
    else
        resized{i,1} = imresize(reg_ims{i}, [NaN, 1920], 'bilinear');
    end
    res_dim(i,:) = size(resized{i});
    clear reg_ims{i};
end
toc

clear reg_ims;
fixed = cell(normal, 1);
fixed_size = zeros(normal, 3);
temp = zeros(1080,1920, 3);
size_diff = 0;
new_ind = i;
for i = 1: normal
    if ((res_dim(i,1) == 1080) && (res_dim(i,2) == 1920))
        fixed{i} = resized{i};
    elseif (reg_long(i) == 1)
        size_diff = res_dim(i,2) - 1920;
        temp = resized{i}(:,(1 + floor(size_diff/2)):(1920 + floor(size_diff/2)), :);
        fixed{i} = temp;
    else
        size_diff = res_dim(i,1) - 1080;
        temp = resized{i}((1 + floor(size_diff/2)):(1080 + floor(size_diff/2)), :, :);
        fixed{i} = temp;
    end
    clear resized{i}
    fixed_size(i,:) = size(fixed{i});
    names{i} = strcat('final/', strrep(names{i}, '.exr', ''), '.pfm');
    write_pfm(fixed{i}, names{i});
end


toc
% This was to check how many images were roughly the same aspect ratio as
% 1080p in the fairchild database, in case you're curious it's 41/105. 
% res_dim (see above) gives more detailed info

% idealnames = cell(ideal, 1);
% idealcount = 1;
% for i = 1: length(files)
%     %aspect(i) = imsizes(i,2)/imsizes(i,1);
%     if 1.7 < aspect(i) && aspect(i) < 1.85
%         idealnames{idealcount} = files(i).name;
%         idealcount = idealcount + 1;
%     end
% end

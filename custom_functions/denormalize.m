function [ out ] = denormalize( in, trough, peak )
%DENORMALISE Summary of this function goes here
%   Detailed explanation goes here

    %trough = 0;
    
    if (~exist('trough', 'var'))
        trough = 0;
    end
    if (~exist('peak', 'var'))
        peak = 1;
    end

    out = (in * (peak - trough)) + trough;

end

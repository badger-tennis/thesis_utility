function [ ] = bar_for_exp( x,y , sceneName)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

b = [1,0,0;0,0,1;0,1,0;0.5,0.5,0;0,0.5,0.5;0.5,0,0.5;0.8,0.4,0.7];
c = zeros(7,3);
superbar(x, 'E', y, 'BarFaceColor', b, 'ErrorbarColor',c, 'ErrorbarRelativeWidth', 0.1)
legend('Diffuse', 'Phong','Walter','Sal Diffuse','Sal Phong','Sal Walter','Ref')

title(sceneName);
ylabel('Mean Z Score');
xlabel('BRDF');
end


files = dir('*.hdr');
numberOfFiles = size(files, 1);
originals = cell(numberOfFiles, 1);
names = cell(numberOfFiles,1);
peak = 0;

for i = 1: numberOfFiles
   originals{i} = hdrread(files(i).name);
   names{i} = strrep(files(i).name, '.exr','');
   
   peak = max(max(max(originals{i})));
   display(sprintf('Peak: %f\n', peak));
   if peak > 500
       new_500 = ClampImg(originals{i}, 0, 500);
   else
       new_500 = originals{i}.*(500/peak);
   end
   if peak > 1000
       new_1000 = ClampImg(originals{i},0, 1000);
   else
       new_1000 = originals{i}.*(1000/peak);
   end
   if peak > 2000
       new_2000 = ClampImg(originals{i},0,2000);
   else
       new_2000 = originals{i}.*(2000/peak);
   end
   if peak > 4000
       new_4000 = ClampImg(originals{i},0,4000);
   else
       new_4000 = originals{i}.*(4000/peak);
   end
   
   
   write_pfm(new_500, strcat('500/', names{i}, '_500.pfm'));
   write_pfm(new_1000, strcat('1000/', names{i}, '_1000.pfm'));
   write_pfm(new_2000, strcat('2000/', names{i}, '_2000.pfm'));
   write_pfm(new_4000, strcat('4000/', names{i}, '_4000.pfm'));
end


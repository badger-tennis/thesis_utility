function [c] = hdrfeatures(filename)
tic
extension = lower(fileExtension(filename));
switch extension
    case 'hdr'
        try
        a1 = hdrimread(filename);
        catch err
            disp('HDR Toolbox failure')
        end
    case 'exr'
        try
            a1 = exrread(filename);
        catch err
            disp('exrread failure')
        end
end

%Declare Necessary Variables
a = lum(a1);
k = 0;
k1 = 0;
b = {};
c = {};
i = 1;
j = 0;
%Scale-Spachay
for k1 = 0:3;
    if k1 ~= 0
        img = imresize(a, 1/(2^k1),'bilinear', 'Antialiasing', false, 'Dither',0, 'Colormap', 'original');
    elseif k1 == 0
        img = a;
    else
        disp('Error in k1')
    end
    for k = 1:4;
        sigma = 2^(k1+(k/4));
        window = ceil(sigma*5);
        h = fspecial('gaussian',window, sigma);
        b{((k1*4)+k)} = imfilter(img, h,'replicate');
        if k ~= 1
            c{i} = b{(k1*4)+k} - b{(k1*4)+(k-1)};
            i = i+1;
        else
        end
end
m = size(c,2);
for j = 1:m
    figure(j);
    imshow(c{j})
end
toc
end

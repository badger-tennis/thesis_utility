function [avgDR, avgMidLum] = footageStats(frame_path, start_frame, frame_step, last_frame, org_extension, dest_extension, display_white_point_max, dest_path)

%% Header information
    avg_mid= 0; avg_dr = 0;
    hdr_filelist = dir(fullfile(frame_path, ['*.' org_extension]));
    nFrames = length(start_frame:frame_step: last_frame);
    
%% processing frames
    parfor i = 1 : nFrames
        s = hdr_filelist(i);
        if(strcmp(org_extension, 'hdr'))
            hdr = hdrread(fullfile(frame_path, s.name));
        elseif(strcmp(org_extension, 'exr'))
            hdr = exrread(fullfile(frame_path, s.name));
        end 

        hdr = ClampImg(hdr, 1e-4, max(hdr(:)));
        y = lum(hdr);   
        avg_mid = avg_mid + (max(y(:))/2);
        avg_dr = avg_dr + log2(max(hdr(:))/min(hdr(:)));
    end 

%% calculation of average mid luminance and average dynamic range

    avgMidLum = ceil(avg_mid/nFrames); 
    avgDR = avg_dr/nFrames;
    fprintf('\nAverage Dynamic Range = %f\n Average mid luma value %f \n', avgDR, avgMidLum);

    multfactor = 2000 / avgMidLum;

    choice = questdlg('Would you like to absolute grade the footage', 'Absolute Gradation', 'Yes', 'No', 'Yes');
    
    switch choice
        case 'Yes'
            absoluteGradeFootage(frame_path, dest_path, nFrames, org_extension, dest_extension, display_white_point_max, multfactor);
            fprintf('\n\n Grading Complete \n\n');
        case 'No'
            fprintf('Footage has not been graded');
    end             
end 

function absoluteGradeFootage(frame_path, dest_path, nFrames, org_ext, dest_ext, max_white, mfact)
%% header information
    minval = 1e-4; maxval = max_white;       
    hdr_filelist = dir(fullfile(frame_path, ['*.' org_ext]));
    
%% processing frames
    parfor i = 1 : nFrames
        s = hdr_filelist(i);        
        if(strcmp(org_ext, 'hdr'))
            hdr = hdrread(fullfile(frame_path, s.name));
        elseif(strcmp(org_ext, 'exr'))
            hdr = exrread(fullfile(frame_path, s.name));
        end 
        
        %% This is just a cross platform replication of pfsabsolute
        hdr_graded = ClampImg((hdr .*mfact), minval, maxval); 
        
        if(strcmp(dest_ext, 'hdr'))
           hdrwrite(hdr_graded, fullfile(dest_path, sprintf('%05d.hdr', (i-1))));
        elseif(strcmp(dest_ext, 'exr'))            
            exrwrite(hdr_graded, fullfile(dest_path, sprintf('%05d.exr', (i-1))));
        end 
    end    
end 
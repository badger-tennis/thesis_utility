function [ dr ] = dynamicRange( im )
%DYNAMICRANGE Summary of this function goes here
%   Detailed explanation goes here
    
%     if trough < 0
%         t = -trough;
%         trough = trough + t;
%         peak = peak + t;
%     end
%     
%     epsilon = 1e-5;
%     
%     if trough < epsilon
%         trough = trough + epsilon;
%         peak = peak + epsilon;
%     end

    peak = max(im(:));
    
    im(im <= 0) = Inf;
    
    trough = min(im(:));
    
    dr = log2(peak / trough);

end

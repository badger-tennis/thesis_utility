function [filesOver1080p] = check_whether_1080p()

%cd(direc);

files = dir('*.pfm');
sizeref = [1080, 1920, 3];
numFiles = length(files);

images = cell(length(files),1);
filesOver1080p = zeros(numFiles, 3);

count = 1;
for i = 1: numFiles
    images{i} = read_pfm(files(i).name);
    
    filesOver1080p(i,:) = size(images{i});
%     size_im = size(images{i});
%     if size_im(1) ~=  sizeref(1) && size_im(2) ~= sizeref(2)...
%             && size_im(3) ~= sizeref(3)
%         filesOver1080p{count} = files(i).name;
%         count = count + 1;
%     end
end

end
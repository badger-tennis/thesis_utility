blue_rubber = fitbrdf('blue-rubber.binary')
brass = fitbrdf('brass.binary')
cherry_235 = fitbrdf('cherry-235.binary')
chrome_steel = fitbrdf('chrome-steel.binary')
colonial_maple = fitbrdf('colonial-maple-223.binary')
color_changing_paint1 = fitbrdf('color-changing-paint1.binary')
color_changing_paint2 = fitbrdf('color-changing-paint2.binary')
color_changing_paint3 = fitbrdf('color-changing-paint3.binary')
dark_blue_paint = fitbrdf('dark-blue-paint.binary')
dark_red_paint = fitbrdf('dark-red-paint.binary')
dark_specular_fabric = fitbrdf('dark-specular-fabric.binary')


filename = 'green-fabric.binary';
gp = fitbrdf(filename)

fid = fopen('C:\Users\bradle_t\Documents\par\64\Par\parameters_green_fabric.txt', 'w');
formatspec = '%s %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f';
fprintf(fid, formatspec, filename, gp);
fclose(fid);
% 
% filename = 'green-plastic.binary';
% gp = fitbrdf(filename)
% 
% fid = fopen('C:\Users\bradle_t\Documents\par\64\Par\parameters_green_plastic.txt', 'w');
% formatspec = '%s %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f';
% fprintf(fid, formatspec, filename, gp);
% fclose(fid);
% 
% filename = 'red-fabric.binary';
% gp = fitbrdf(filename)
% 
% fid = fopen('C:\Users\bradle_t\Documents\par\64\Par\parameters_red_plastic.txt', 'w');
% formatspec = '%s %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f';
% fprintf(fid, formatspec, filename, gp);
% fclose(fid);
% 
% filename = 'alum-bronze.binary';
% gp = fitbrdf(filename)
% 
% fid = fopen('C:\Users\bradle_t\Documents\par\64\Par\parameters_alum_bronze.txt', 'w');
% formatspec = '%s %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f';
% fprintf(fid, formatspec, filename, gp);
% fclose(fid);
% 
% filename = 'violet-rubber.binary';
% gp = fitbrdf(filename)
% 
% fid = fopen('C:\Users\bradle_t\Documents\par\64\Par\parameters_violet_rubber.txt', 'w');
% formatspec = '%s %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f';
% fprintf(fid, formatspec, filename, gp);
% fclose(fid);
% 


% filename = 'blue-fabric.binary';
% gp = fitbrdf(filename)
% 
% fid = fopen('C:\Users\bradle_t\Documents\par\64\Par\parameters_blue_fabric.txt', 'w');
% formatspec = '%s %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f';
% fprintf(fid, formatspec, filename, gp);
% fclose(fid);
% 
% filename = 'chrome.binary';
% gp = fitbrdf(filename)
% 
% fid = fopen('C:\Users\bradle_t\Documents\par\64\Par\parameters_chrome.txt', 'w');
% formatspec = '%s %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f';
% fprintf(fid, formatspec, filename, gp);
% fclose(fid);
% 
% filename = 'red-plastic.binary';
% gp = fitbrdf(filename)
% 
% fid = fopen('C:\Users\bradle_t\Documents\par\64\Par\parameters_red_plastic.txt', 'w');
% formatspec = '%s %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f %2.5f';
% fprintf(fid, formatspec, filename, gp);
% fclose(fid);
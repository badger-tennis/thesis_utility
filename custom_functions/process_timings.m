function [a]= process_timings(filename)

fid = fopen(filename, 'r');
formatspec = '%f %s %s %f %f';

a = fscanf(fid,formatspec);

end
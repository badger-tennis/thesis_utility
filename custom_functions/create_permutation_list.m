function [permutation_list, fileNameContainer] = create_permutation_list(directoryNames, numberOfExposures, numParticipants)

%seriously = textFile;
%permutation_list = seriously;

% if length(directoryNames) ~= numberOfExposures
%     msg = 'Number of directories does not match expected number of exposures';
%     error(msg);
% end

%verify that each directory contains the same number of images
check_contents_of_directories(directoryNames);

%creates a list of which image should be exposed at which level for each
%participant
matrix = create_matrix_80(numParticipants);

% prep for assigning image names
home = cd;
cd(directoryNames{1});
numberImages = length(dir('*.pfm'));
cd(home);
fileNameContainer = cell(numberImages, length(directoryNames));

for i = 1: length(directoryNames);
    cd(directoryNames{i});
    files = dir('*.pfm');
    for j = 1: numberImages
        fileNameContainer{j, i} = files(j).name;
    end
    cd(home);
end

filePathMatrix = cell(numberImages, numParticipants);

for j = 1: numParticipants
    for i = 1: numberImages
        filePathMatrix{i, j} = strcat(directoryNames{matrix(i,j)}, '/', ...
            fileNameContainer{i,matrix(i,j)});
    end
end

imagesSortedByExp = cell(numberOfExposures, numParticipants);
for i = 1: numParticipants
    for j = 1: numberOfExposures
        imagesSortedByExp{j,i} = cell((numberImages/numberOfExposures), 1);
    end
end

%CHANGE THIS SECTION IF YOU CHANGE NUMBER OF EXPOSURES, I GOT LAZY HERE
%counters = zeros(numberOfExposures);
for j = 1:numParticipants
    counters = ones(numberOfExposures, 1);
    for i = 1: numberImages
        if matrix(i,j) == 1
            imagesSortedByExp{1, j}{counters(1)} = filePathMatrix{i,j};
            counters(1) = counters(1) + 1;
        elseif matrix(i,j) == 2
            imagesSortedByExp{2, j}{counters(2)} = filePathMatrix{i,j};
            counters(2) = counters(2) + 1;
        elseif matrix(i,j) == 3
            imagesSortedByExp{3, j}{counters(3)} = filePathMatrix{i,j};
            counters(3) = counters(3) + 1;
        elseif matrix(i,j) == 4
            imagesSortedByExp{4, j}{counters(4)} = filePathMatrix{i,j};
            counters(4) = counters(4) + 1;
        end
    end
end

groupPermutations1 = funk_up_permutations(4);
groupPermutations = [groupPermutations1;groupPermutations1]';

finalList = cell(numberImages, numParticipants);

for j = 1: numParticipants
    a = randperm(numberImages/numberOfExposures);
    b = randperm(numberImages/numberOfExposures);
    c = randperm(numberImages/numberOfExposures);
    d = randperm(numberImages/numberOfExposures);
    for i = 1: numberImages/numberOfExposures
        finalList{i,j} = imagesSortedByExp{groupPermutations(1,j),j}{a(i)};
        finalList{i+20,j} = imagesSortedByExp{groupPermutations(2,j),j}{b(i)};
        finalList{i+40,j} = imagesSortedByExp{groupPermutations(3,j),j}{c(i)};
        finalList{i+60,j} = imagesSortedByExp{groupPermutations(4,j),j}{d(i)};
    end
end

p = cell(numParticipants,1);
for i = 1: numParticipants
    p{i} = sprintf('p%02d.txt', i);
end

formatspec = '%s\n';
%formatspecStart = '%s\n';
for j = 1: numParticipants
    path = strcat('C:/Users/bradle_t/Documents/MATLAB/image_lists/', p{j});
    fid = fopen(path, 'w');
    %%fprintf(fid, formatspecStart, 'gray.pfm');
    for i = 1: numberImages
        fprintf(fid, formatspec, finalList{i,j});
    end
    fclose(fid);
end

permutation_list = finalList;

end
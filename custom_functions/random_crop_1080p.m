function [cropped] = random_crop_1080p(im)

size_im = size(im);

indexh = 0;
indexw = 0;
cropped = zeros(1080,1920, 3);
if (size_im(1) > 1080) || (size_im(2) > 1920)
    indexh = randi(size_im(1)-1079);
    indexw = randi(size_im(2)-1919);
    cropped = im(indexh:(indexh + 1079), indexw:(indexw + 1919), :);
else
    cropped = im;
end
    
end
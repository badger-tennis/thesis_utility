b = randperm(80);
c = zeros(80,4);
d = 1:4;

j = 0;
k = 0;
l = 0;
m = 0;
counter = ones(80,1);

for i = 1: 80
    c(b(i), :) = randperm(4);
end

i = 1;
while i ~= 80
   if c(b(i), counter(b(i))) == 1 && j < 20
       j = j + 1;
       i = i + 1;
   elseif c(b(i),counter(b(i))) == 2 && k < 20
       k = k + 1;
       i = i + 1;
   elseif c(b(i), counter(b(i))) == 3 && l < 20
       l = l+1;
       i = i + 1;
   elseif c(b(i), counter(b(i))) == 4 && m < 20
       m = m + 1;
       i = i + 1;
   else
       for f = (80 - i): 80
          c(b(f), :)  = randperm(4);
          
       end
       counter(b(i)) = counter(b(i)) - 1;
   end           
   counter(b(i)) =  counter(b(i)) + 1;
end

%   1. n groups

%   2. m images

%   3. p participants

%   4. m % n == 0 

%   5. each image, i, is in each of n groups before it is placed in the
%   same group twice

%   6. total images assigned (ti) = m * p

%   7. images per group (gi) = m/n

%   8. p = 2*n!

%   9. ti = m * 2 * n!

%   10. try counting each element to 12 for each group

%   assign each picture to all groups necessary, randomise order and
%   participant
                        
function [im, ymax] = calibrate_to_percentile(image, brightness, percent)

image(image < 0) = 0;

yuv = ConvertRGBtoYUV(image,0);

y = yuv(:, :, 1);

%lu = max(y(:));
lu = MaxQuart(y, percent);

newY = yuv ./ lu;

newY(newY > 1) = 1;

newY = newY .* brightness;

yuv = newY; %yuv(:,:,1) = newY;

assert(max(yuv(:) <= brightness));

im = ConvertRGBtoYUV(yuv, 1);

ymax = max(max(yuv(:,:,1)));
%assert(max(im(:)) <= (brightness+1000));


%im = im .^ (1/1.25);


%lu = MaxQuart(yuv(:,:, 1), percent);
%im_max = max(yuv(:));
%im_min = max(min(yuv(:)), 0);
%temp1 = normalize(yuv(:,:,1), im_min, lu);
%temp1(temp1 < 0) = 0;
%temp1(temp1 > 1) = 1;
%yuv(:,:,1) = denormalize(temp1, im_min, brightness);
%%yuv(:,:,1) = yuv(:,:,1).*(brightness/lu)
%yuv(yuv>brightness) = brightness;

%im = ConvertRGBtoYUV(yuv, 1);


end
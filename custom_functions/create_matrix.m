function [matrix] = create_matrix(columns, rows, groups)

matrix = zeros(rows, columns);
oneToRows = 1:rows;
randomOrderRows = randperm(rows);

currentColumn = 1;
for i = 1: rows
    matrix(i, currentColumn) = ceil(randomOrderRows(i)/(rows/groups));
end
end
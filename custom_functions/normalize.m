function [ out, trough, peak ] = normalize( in, trough, peak )
%NORMALISE Summary of this function goes here
%   Detailed explanation goes here

    trough = 0;

    if (~exist('trough', 'var'))
        trough = min(in(:));
    end
    if (~exist('peak', 'var'))
        peak = max(in(:));
    end
    
    out = (in - trough) ./ (peak - trough);
    
end

files = dir('*.hdr');
numberOfFiles = length(files);
originals = cell(numberOfFiles, 1);
names = cell(numberOfFiles,1);
peak = 0;

for i = 1: numberOfFiles
   originals{i} = hdrread(files(i).name);
   names{i} = strrep(files(i).name, '.hdr','');
   
   p95th = MaxQuart(originals{i},0.95);
   p99th = MaxQuart(originals{i},0.99);
   peak = max(max(max(originals{i})));
   
   display(sprintf('Peak: %f\n', peak));
   display(sprintf('95th: %f\n', p95th));
   display(sprintf('99th: %f\n', p99th));
   
   new_500 = originals{i}.*(500/p95th);
   new_1000 = originals{i}.*(1000/p95th);
   new_2000 = originals{i}.*(2000/p95th);
   new_4000 = originals{i}.*(4000/p95th);
   
%    new_500 = originals{i}.*(500/p99th);
%    new_1000 = originals{i}.*(1000/p99th);
%    new_2000 = originals{i}.*(2000/p99th);
%    new_4000 = originals{i}.*(4000/p99th);
   
%    new_500 = originals{i}.*(500/peak);
%    new_1000 = originals{i}.*(1000/peak);
%    new_2000 = originals{i}.*(2000/peak);
%    new_4000 = originals{i}.*(4000/peak);

   new_500(new_500>500) = 500;
   new_1000(new_1000>1000) = 1000;
   new_2000(new_2000>2000) = 2000;
   new_4000(new_4000>4000) = 4000;
   
   write_pfm(new_500, strcat('500/', names{i}, '_500.pfm'));
   write_pfm(new_1000, strcat('1000/', names{i}, '_1000.pfm'));
   write_pfm(new_2000, strcat('2000/', names{i}, '_2000.pfm'));
   write_pfm(new_4000, strcat('4000/', names{i}, '_4000.pfm'));
end


function [list] = funk_up_permutations(number)

permutations = perms(1:number);
count = 1;
list = zeros(length(permutations),number);
index = 1;
resetTo = 2;

while count <= length(permutations)
    list(count,:) = permutations(index,:);
    index = index + 6;
    if index > 24
        index = resetTo;
        resetTo = resetTo + 1;
    end
    count = count + 1;
end
function [img] = test(filename)
extension = lower(fileExtension(filename));
switch extension
    case 'hdr'
        try
        a = hdrimread(filename);
        catch err
            disp('HDR Toolbox failure')
        end
    case 'exr'
        try
            a = exrread(filename);
        catch err
            disp('exrread failure')
        end
end
img = a;

end
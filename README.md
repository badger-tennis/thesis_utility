# thesis_utility

In case this repo ever goes public.

This is basically the matlab functions I wrote for my thesis and a couple of academic papers. 
There are a few I used and didn't write, mostly in the custom functions folder.

I doubt much of the code would be of any use to many people, unless they're overly concerned with
eye-tracking or saliency or interpreting raw data from Facelab 4.5.

There is also most likely a lot of stuff about fitting brdfs to the MERL database in one of the folders. 
which is probably the only stuff that has a chance of being useable.

Anyway, a lot of this code is dependent on the HDR Toolbox, Facelab's software and the MIT saliency
toolbox (easily googleable). So while I don't mind if anyone takes and re-uses this code, I can't
imagine that it'd be an easy task.

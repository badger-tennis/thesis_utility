function [newres] = correct_perclos_processing(results)
% This function orders the perclos results returned by process_perclos.m so
% that the results are ordered by brightness and order that the brightness
% group was displayed in

%numP = size(results);
%numP = numP(1);
%newres = zeros((4*numP),4);

a = perclos_sort_exp(results, 1);
b = perclos_sort_exp(results, 2);
c = perclos_sort_exp(results, 3);
d = perclos_sort_exp(results, 4);

newres = [a;b;c;d];



end
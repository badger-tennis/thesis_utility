function [retparticipants] = initialise_participants(numParticipants)

retparticipants = cell(numParticipants,1);

for i = 1: numParticipants
    retparticipants{i} = Participant;
end


end
function [kmaps] = k_fold_maps(k, numImages, maps, x_res, y_res)

w = numImages/k;

kmaps = cell(w, 2);
blank = zeros(y_res,x_res);

for p = 1: w
    kmaps{p,1} = blank;
    kmaps{p,2} = blank;
end
    
for i = 1: w
    %disp('innergroup:')
    for j = (i*k-(k-1)): i*k
        %disp(j)
        kmaps{i,1} = kmaps{i,1} + maps{j};
    end
    %disp('outergroup:')
    if i ==1
        for j = (k+1): numImages
            %disp(j)
            kmaps{i,2} = kmaps{i,2} + maps{j};
        end
    else
        for j  = 1: (i*k)-k
            %disp(j)
            kmaps{i,2} = kmaps{i,2} + maps{j};
        end
        for j = (i*k)+1 : numImages
            %disp(j)
            kmaps{i,2} = kmaps{i,2} + maps{j};
        end
    end
    kmaps{i,1}(kmaps{i,1}>1)=1;
    kmaps{i,2}(kmaps{i,2}>1)=1;
end
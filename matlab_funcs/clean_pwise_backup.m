function [newret] = clean_pwise_backup(ret)

dim = size(ret);
numP = dim(2)/3;
a = ret(:, 1:numP);
b = ret(:, (numP+1):(2*numP));
c = ret(:, ((2*numP) + 1): (3*numP));
nanners = ~(isnan(ret(:, (1:numP))));
% 
% 
% newA = a(a(nanners));
% newB = b(b(nanners));
% newC = c(c(nanners));
% 
% newret = [newA, newB, newC];
stupidInd = find(nanners);
newSize = 3*(numP - length(stupidInd));
thirdnewSize = newSize/3;

newret = zeros(dim(1), newSize);

x = 1;
count = 1;
legit = 1;
while (count < thirdnewSize)
    if ~(nanners(count))
        newret(:, legit) = ret(:, count);
        newret(:, (thirdnewSize + legit)) = ret(:, (numP + count));
        newret(:, ((2*thirdnewSize) + legit)) = ret(:, ((2*numP) + count));
        legit = legit + 1;
    else 
        x = x + 1;
    end
    count = count + 1;
end
end
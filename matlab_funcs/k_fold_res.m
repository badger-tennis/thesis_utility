function [emd, sim, roc, ccs, nss, kld, interemd, intersim, interroc, interccs, internss, interkld] = k_fold_res(k, single_imdata, g_win_width, x_res, y_res, seconds)

%g_win_width = 118;
%x_res = 1920;
%y_res = 1080;

%maps = cell(4,1);


fix_thresh = 34; % 34 calculated by hand based on Engelke, Maeder, Zepernick paper 
%"visual attention modelling for subjective image quality databases"

min_points = 4; % from same paper

%new_im = single_imdata;
%numP = length(single_imdata.indices);

% find where each exposure is

temp = (single_imdata.exposure_levels == 1);
ind_1 = find(temp);
temp = (single_imdata.exposure_levels == 2);
ind_2 = find(temp);
temp = (single_imdata.exposure_levels == 3);
ind_3 = find(temp);
temp = (single_imdata.exposure_levels == 4);
ind_4 = find(temp);

[emd1, sim1, roc1, ccs1, nss1, kld1, smooth1, kmaps1] = single_kfold(k, single_imdata, ind_1, min_points, fix_thresh, g_win_width, x_res, y_res, seconds);
[emd2, sim2, roc2, ccs2, nss2, kld2, smooth2, kmaps2] = single_kfold(k, single_imdata, ind_2, min_points, fix_thresh, g_win_width, x_res, y_res, seconds);
[emd3, sim3, roc3, ccs3, nss3, kld3, smooth3, kmaps3] = single_kfold(k, single_imdata, ind_3, min_points, fix_thresh, g_win_width, x_res, y_res, seconds);
[emd4, sim4, roc4, ccs4, nss4, kld4, smooth4, kmaps4] = single_kfold(k, single_imdata, ind_4, min_points, fix_thresh, g_win_width, x_res, y_res, seconds);

smooth = {smooth1, smooth2, smooth3, smooth4};
kmaps = {kmaps1, kmaps2, kmaps3, kmaps4};



intersize = size(kmaps{1});
interemd = cell(4, 1);
intersim = cell(4, 1);
interroc = cell(4, 1);
interccs = cell(4, 1);
internss = cell(4, 1);
interkld = cell(4, 1);

for i = 2:4
    interemd{i} = zeros(intersize(1));
    intersim{i} = zeros(intersize(1));
    interroc{i} = zeros(intersize(1));
    interccs{i} = zeros(intersize(1));
    internss{i} = zeros(intersize(1));
    interkld{i} = zeros(intersize(1));
    for n = 1: intersize(1)
        for j = 1:intersize(1)
%             interemd{i-1}(j, n) = EMD(smooth{1}{n, 2}, smooth{i}{j, 1}, 0, 64);
%             intersim{i-1}(j, n) = similarity(smooth{1}{n, 2}, smooth{i}{j, 1}, 0);
%             [interroc{i-1}(j, n), ~, ~, ~] = AUC_Judd(smooth{1}{n, 2}, kmaps{i}{j, 1}, 1, 0);
%             interccs{i-1}(j, n) = CC(smooth{1}{n, 2}, smooth{i}{j, 1});
%             internss{i-1}(j, n) = NSS(smooth{1}{n, 2}, kmaps{i}{j, 1});
            kla = KLdiv(smooth{1}{n, 2}, smooth{i}{j, 1});
            klb = KLdiv(smooth{i}{j, 1}, smooth{1}{n, 2});
            interkld{i-1}(j, n) = (kla+klb)/2;
        end
    end
end

%res = [res1, res2, res3, res4];
emd = [emd1, emd2, emd3, emd4];
sim = [sim1, sim2, sim3, sim4];
roc = [roc1, roc2, roc3, roc4];
ccs = [ccs1, ccs2, ccs3, ccs4];
nss = [nss1, nss2, nss3, nss4];
kld = [kld1, kld2, kld3, kld4];
end





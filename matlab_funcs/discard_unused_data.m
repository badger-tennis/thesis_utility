function [data] = discard_unused_data(test_data, eye_logs)

[sync_start, sync_end] = sync_data(test_data, eye_logs);
numParticipants = length(test_data);
data = test_data;
for i = 1: numParticipants
    data{i}.frame_num = test_data{i}.frame_num(sync_start(i):sync_end(i));
    data{i}.experiment_time = test_data{i}.experiment_time(sync_start(i):sync_end(i));
    data{i}.gmt_s = test_data{i}.gmt_s(sync_start(i):sync_end(i));
    data{i}.gmt_ms = test_data{i}.gmt_ms(sync_start(i):sync_end(i));
    data{i}.delay = test_data{i}.delay(sync_start(i):sync_end(i));
    data{i}.annotation_id = test_data{i}.annotation_id(sync_start(i):sync_end(i));
    data{i}.track_state = test_data{i}.track_state(sync_start(i):sync_end(i));
    data{i}.model_qual = test_data{i}.model_qual(sync_start(i):sync_end(i));
    data{i}.right_eye_close = test_data{i}.right_eye_close(sync_start(i):sync_end(i));
    data{i}.left_eye_close = test_data{i}.left_eye_close(sync_start(i):sync_end(i));
    data{i}.right_close_conf = test_data{i}.right_close_conf(sync_start(i):sync_end(i));
    data{i}.left_close_conf = test_data{i}.left_close_conf(sync_start(i):sync_end(i));
    data{i}.eye_close_calib = test_data{i}.eye_close_calib(sync_start(i):sync_end(i));
    data{i}.blinking = test_data{i}.blinking(sync_start(i):sync_end(i));
    data{i}.blink_freq = test_data{i}.blink_freq(sync_start(i):sync_end(i));
    data{i}.blink_duration = test_data{i}.blink_duration(sync_start(i):sync_end(i));
    data{i}.perclos = test_data{i}.perclos(sync_start(i):sync_end(i));
    data{i}.gaze_quality_r = test_data{i}.gaze_quality_r(sync_start(i):sync_end(i));
    data{i}.gaze_quality_l = test_data{i}.gaze_quality_l(sync_start(i):sync_end(i));
    data{i}.gaze_calibrated = test_data{i}.gaze_calibrated(sync_start(i):sync_end(i));
    data{i}.saccade = test_data{i}.saccade(sync_start(i):sync_end(i));
    data{i}.gaze_scr_intsect = test_data{i}.gaze_scr_intsect(sync_start(i):sync_end(i));
    data{i}.gsi_pixel_x = test_data{i}.gsi_pixel_x(sync_start(i):sync_end(i));
    data{i}.gsi_pixel_y = test_data{i}.gsi_pixel_y(sync_start(i):sync_end(i));
    data{i}.hpos_conf = test_data{i}.hpos_conf(sync_start(i):sync_end(i));
end
function [] = write_perclos_csv(results)

titleformat = '%s,%s,%s,%s,%s\n';
fid = fopen('perc_results.txt', 'w');
fprintf(fid, titleformat, 'one', 'two', 'three', 'four', 'first');

resultspec = '%f,%f,%f,%f,%d\n';
numP = length(results);

for i = 1: numP
    if ~(isnan(results(i,1)))
        fprintf(fid, resultspec, results(i,1), results(i,2), results(i,3), results(i,4), results(i,5));
    end
end

fclose(fid)
end
function [map] = fdm(pixel_x, pixel_y, x_res, y_res, g_win_width, fix_thresh, min_points)

% too tired to do this properly so change this if you wanna use different
% image sizes
im = zeros(y_res, x_res);

numPoints = length(pixel_x);
temp_clusters = cell(numPoints, 1);
if isempty(pixel_x)
    disp('fuck');
    map = im;
else

count = 1;
temp_clusters{1} = [pixel_y(1), pixel_x(1)];
for i = 2: numPoints
    point = [pixel_y(i), pixel_x(i)];
    uj = mean([temp_clusters{count};point]);
    dist = sqrt(((point(1) - uj(1))^2) + ((point(2) - uj(2))^2));
    if dist < fix_thresh        
        temp_clusters{count} = [temp_clusters{count}; point];
    else
        count = count + 1;
        temp_clusters{count} = point;
    end
end

%might want to replace this with '@size' and make adjustments
temp = cellfun(@length, temp_clusters);
temp2 = (temp>min_points);
ind_fix = find(temp2);
num_fix = length(ind_fix);
fixations = zeros(num_fix, 2);
fix_lengths = zeros(num_fix, 1);

%count = 1;
for i = 1: num_fix
    fixations(i,:) = mean(temp_clusters{ind_fix(i)});
    fix_lengths(i) = length(temp_clusters{ind_fix(i)});
end

for i = 1: num_fix
    im(round(fixations(i,1)), round(fixations(i,2))) = 1;%fix_lengths(i);
end

 
%sigma = 45;%g_win_width/2;
%temp_map = imgaussfilt(im, sigma, 'FilterSize', (2*g_win_width+1));
%peak = max(max(temp_map));
%map = temp_map./peak;
% yesssssssssss it's done
map = im;
end
end
function [] = create_fdm_vis(im, maps, name)

peak = max(max(max(im)));
tone = ReinhardTMO(im, 0.5, peak);

a = tone;
b = tone;
c = tone;
d = tone;

a(:,:,1) = maps{1}.*2;
b(:,:,1) = maps{2}.*2;
c(:,:,1) = maps{3}.*2;
d(:,:,1) = maps{4}.*2;

imwrite(tone, strcat(name, 'TMO.png'));
imwrite(a, strcat(name, '500.png'));
imwrite(b, strcat(name, '1000.png'));
imwrite(c, strcat(name, '2000.png'));
imwrite(d, strcat(name, '4000.png'));
imlum = lum(im);
lumwrite = imlum./4000;
imwrite(lumwrite, strcat(name, 'Lum.png'));
% now go fuck yourself

end
function [groups] = check_order_dist(imdata)

groups = zeros(80,4);

for i =1: 80
    for j = 1:48
        if (imdata{i}.indices(j) <= 20)
            groups(i, 1) = groups(i,1) + 1;
        elseif (20 < imdata{i}.indices(j) && imdata{i}.indices(j) <= 40)
            groups(i,2) = groups(i,2) +1;
        elseif (40 < imdata{i}.indices(j)&& imdata{i}.indices(j) <= 60)
            groups(i,3) = groups(i,3) +1;
        elseif (60 < imdata{i}.indices(j)&& imdata{i}.indices(j) <= 80)
            groups(i,4) = groups(i,4) +1;
        end
    end
end

end
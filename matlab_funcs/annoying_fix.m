function [image_list] = annoying_fix(imageNames)

image_list = cell(length(imageNames),1);
for i = 1: length(imageNames)
    image_list{i} = strrep(imageNames{i}, '/','');
end
end
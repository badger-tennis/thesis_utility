function [groups] = check_exp_dist(imdata)

groups = zeros(80,4);

for i =1: 80
    for j = 1:48
        if (imdata{i}.exposure_levels(j)==1)
            groups(i, 1) = groups(i,1) + 1;
        elseif imdata{i}.exposure_levels(j)==2
            groups(i,2) = groups(i,2) +1;
        elseif imdata{i}.exposure_levels(j)==3
            groups(i,3) = groups(i,3) +1;
        elseif imdata{i}.exposure_levels(j)==4
            groups(i,4) = groups(i,4) +1;
        end
    end
end

end
function [eye_time_data] = load_eye_logs(logpath, formatspec)

%numImages = 80;

cd(logpath);
file_list = dir('*txt');
numParticipants = length(file_list);
eye_time_data = cell(numParticipants, 3);

for i = 1: numParticipants
    fid = fopen(file_list(i).name);
    scanned = textscan(fid, formatspec);
    for j = 1:length(scanned)
        eye_time_data{i, j} = scanned{j};
    end
    fclose(fid);
end
cd ..
end
function [ret] = perclos_p_wise(im_data, participants)
    numI = length(im_data);
    numP = length(participants);
    ret = zeros(numI, 6);
    means = zeros(numP, 1);
    stddv = zeros(numP, 1);
    percret = zeros(numI, numP);
    indret = zeros(numI, numP);
    permret = zeros(numI, numP);
    
    for i = 1: numP
        means(i) = mean(participants{i}.perclos);
        stddv(i) = std(participants{i}.perclos);
        perc = participants{i}.perclos;
        perc = perc - means(i);
        perc = perc./stddv(i);
        
        for j = 1: numI
            percret(j, i) = mean(perc(participants{i}.sync_times(im_data{j}.indices(i),1):participants{i}.sync_times(im_data{j}.indices(i),2)));
            indret(j,i) = im_data{j}.indices(i);
            if (im_data{j}.indices <= 20)
                permret(j,i) = participants{i}.permutation(1);
            elseif (20 < im_data{j}.indices(i) <= 40)
                permret(j,i) = participants{i}.permutation(2);
            elseif (40 < im_data{j}.indices(i) <= 60)
                permret(j,i) = participants{i}.permutation(3);
            elseif (60 < im_data{j}.indices(i) <= 80)
                permret(j,i) = participants{i}.permutation(3);
            end
        end
    end
ret = [percret, indret, permret];
end
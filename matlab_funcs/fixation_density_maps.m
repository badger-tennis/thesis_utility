function [maps] = fixation_density_maps(single_imdata, g_win_width, x_res, y_res, seconds)

maps = cell(4,1);
fix_thresh = 34; % 34 calculated by hand based on Engelke, Maeder, Zepernick paper 
%"visual attention modelling for subjective image quality databases"

min_points = 4; % from same paper

%new_im = single_imdata;
%numP = length(single_imdata.indices);

% find where each exposure is

temp = (single_imdata.exposure_levels == 1);
ind_1 = find(temp);
temp = (single_imdata.exposure_levels == 2);
ind_2 = find(temp);
temp = (single_imdata.exposure_levels == 3);
ind_3 = find(temp);
temp = (single_imdata.exposure_levels == 4);
ind_4 = find(temp);

maps{1} = single_map(single_imdata, ind_1, min_points, fix_thresh, g_win_width, x_res, y_res, seconds);
maps{2} = single_map(single_imdata, ind_2, min_points, fix_thresh, g_win_width, x_res, y_res, seconds);
maps{3} = single_map(single_imdata, ind_3, min_points, fix_thresh, g_win_width, x_res, y_res, seconds);
maps{4} = single_map(single_imdata, ind_4, min_points, fix_thresh, g_win_width, x_res, y_res, seconds);

end




% %pixel_x = cell(length(ind_1),1);
% %pixel_y = cell(length(ind_1),1);
% num1s = length(ind_1);
% maps1 = cell(num1s,1);
% for i = 1: num1s
%     % Here beginneth risky logic shite
%     log_check = ((single_imdata.blinking{ind_1(i)} == 0) & (single_imdata.saccade{ind_1(i)} == 0) ...
%         & (single_imdata.gaze_scr_intsect{ind_1(i)} == 1));
%     pixel_x = single_imdata.gsi_pixel_x{ind_1(i)}(log_check);
%     pixel_y = single_imdata.gsi_pixel_y{ind_1(i)}(log_check);
%     maps1{i} = fdm(pixel_x, pixel_y, x_res, y_res, g_win_width, fix_thresh, min_points);
% end
% 
% fin1 = zeros(y_res, x_res);
% for i = 1: num1s
%     fin1 = fin1 + maps1{i};
% end
% %fin1 = fin1./num1s;
% 
%     %sigma = g_win_width/2;
%     %filt = fspecial('gaussian', g_win_width, sigma);
%     %temp_map = imfilter(fin1, filt);
%     %peak = max(max(temp_map));
%     %maps{1} = temp_map./peak;
%     
% peak = max(max(fin1));
% if (peak >1.1)
%     err('two fixations in same place');
% end
% maps{1} = fin1;
% %maps{1} = fin1./peak;
% 
% num2s = length(ind_2);
% maps2 = cell(num2s,1);
% for i = 1: num2s
%     % Here beginneth risky logic shite
%     log_check = ((single_imdata.blinking{ind_2(i)} == 0) & (single_imdata.saccade{ind_2(i)} == 0) ...
%         & (single_imdata.gaze_scr_intsect{ind_2(i)} == 1));
%     pixel_x = single_imdata.gsi_pixel_x{ind_2(i)}(log_check);
%     pixel_y = single_imdata.gsi_pixel_y{ind_2(i)}(log_check);
%     maps2{i} = fdm(pixel_x, pixel_y, x_res, y_res, g_win_width, fix_thresh, min_points);
% end
% 
% fin2 = zeros(y_res, x_res);
% for i = 1: num2s
%     fin2 = fin2 + maps2{i};
% end
% %fin2 = fin2./num2s;
% 
%     %sigma = g_win_width/2;
%     %filt = fspecial('gaussian', g_win_width, sigma);
%     %temp_map = imfilter(fin2, filt);
%     %peak = max(max(temp_map));
%     %maps{2} = temp_map./peak;
%     
% peak = max(max(fin2));
% if (peak >1.1)
%     err('two fixations in same place');
% end
% maps{2} = fin2;
% %maps{2} = fin2./peak;
% 
% num3s = length(ind_3);
% maps3 = cell(num3s,1);
% for i = 1: num3s
%     % Here beginneth risky logic shite
%     log_check = ((single_imdata.blinking{ind_3(i)} == 0) & (single_imdata.saccade{ind_3(i)} == 0) ...
%         & (single_imdata.gaze_scr_intsect{ind_3(i)} == 1));
%     pixel_x = single_imdata.gsi_pixel_x{ind_3(i)}(log_check);
%     pixel_y = single_imdata.gsi_pixel_y{ind_3(i)}(log_check);
%     maps3{i} = fdm(pixel_x, pixel_y, x_res, y_res, g_win_width, fix_thresh, min_points);
% end
% 
% fin3 = zeros(y_res, x_res);
% for i = 1: num3s
%     fin3 = fin3 + maps3{i};
% end
% %fin3 = fin3./num3s;
% 
%     %sigma = g_win_width/2;
%     %filt = fspecial('gaussian', g_win_width, sigma);
%     %temp_map = imfilter(fin3, filt);
%     %peak = max(max(temp_map));
%     %maps{3} = temp_map./peak;
% 
% peak = max(max(fin3));
% if (peak >1.1)
%     err('two fixations in same place');
% end
% maps{3} = fin3;
% %maps{3} = fin3./peak;
% 
% num4s = length(ind_4);
% maps4 = cell(num4s,1);
% for i = 1: num4s
%     % Here beginneth risky logic shite
%     log_check = ((single_imdata.blinking{ind_4(i)} == 0) & (single_imdata.saccade{ind_4(i)} == 0) ...
%         & (single_imdata.gaze_scr_intsect{ind_4(i)} == 1));
%     pixel_x = single_imdata.gsi_pixel_x{ind_4(i)}(log_check);
%     pixel_y = single_imdata.gsi_pixel_y{ind_4(i)}(log_check);
%     maps4{i} = fdm(pixel_x, pixel_y, x_res, y_res, g_win_width, fix_thresh, min_points);
% end
% 
% fin4 = zeros(y_res, x_res);
% for i = 1: num4s
%     fin4 = fin4 + maps4{i};
% end
% %fin4 = fin4./num4s;
%     %sigma = g_win_width/2;
%     %filt = fspecial('gaussian', g_win_width, sigma);
%     %temp_map = imfilter(fin4, filt);
%     %peak = max(max(temp_map));
%     %maps{4} = temp_map./peak;
% 
% peak = max(max(fin4));
% if (peak >1.1)
%     err('two fixations in same place');
% end
% maps{4} = fin4;
% %maps{4} = fin4./peak;



function [sync_times, retparticipants] = sync_images(data, eye_logs, participants)

x = size(eye_logs);
numP = x(1);
numI = length(eye_logs{1,1});
sync_times = cell(numP,1);

for i = 1: numP
    sync_times{i} = zeros(numI, 2);
    data_entries = length(data{i}.gmt_s);
    participants{i}.ptime = (data{i}.gmt_s.*1000) + data{i}.gmt_ms;
    for j = 1: numI
        % debug test
        %if ((i == 14) && (j == 80))
         %   x = 4;
        %end
        
        sync_times{i}(j,1) = (find((participants{i}.ptime)>(eye_logs{i,2}(j)),1)) - 1;
        if (participants{i}.ptime(data_entries) ==  eye_logs{i,3}(j))
            sync_times{i}(j,2) = data_entries;
        else
            sync_times{i}(j,2) = (find((participants{i}.ptime)>(eye_logs{i,3}(j)),1)) - 1;
        end
    end
    participants{i}.sync_times = sync_times{i};
    participants{i}.image_order = get_image_names(eye_logs{i,1}, 0);
end
retparticipants = participants;
% ind = 3;
% % eye_log{1} = eye_logs{ind, 1};
% % eye_log{2} = eye_logs{ind, 2};
% % eye_log{3} = eye_logs{ind, 3};
% % 
% % test_data = data{ind};
% 
% ret = cell(size(eye_log));
% 
% data_entries = length(test_data.gmt_s);
% log_entries = length(eye_log{1});
% ptime = zeros(data_entries,1);
% 
% for i = 1: data_entries
%     ptime(i) = (test_data.gmt_s(i)*1000 + test_data.gmt_ms(i));
% end
% 
% count = 1;
% while ptime(count) < eye_log{2}(1)
%     count = count + 1;
% end
% 
% over_diff = ptime(count) - eye_log{2}(1);
% under_diff = eye_log{2}(1) - ptime(count-1);
% 
% under = 0;
% if under_diff < over_diff
%     count = count -1;
%     under = 1;
%     diff = eye_log{2}(1) - ptime(count);
% else 
%     diff = ptime(count) - eye_log{2}(1);
% end
% 
% sync_start = count;
% % a = ptime(count)
% % b = eye_log{2}(1)
% % c = diff
end
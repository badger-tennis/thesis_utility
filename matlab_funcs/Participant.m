classdef Participant
    properties
        name
        perclos
        permutation = zeros(4,1);
        image_order
        ptime
        sync_times
    end
    methods
        function obj = set.name(obj, pname)
            if ~(ischar(pname))
                msg = 'Participant name must be a string';
                error(msg);
            end
        end
    end
end
function [level] = parse_exposure(character)

temp = str2double(character);
if temp == 1
    level = 2;
elseif temp == 2
    level = 3;
elseif temp == 4
    level = 4;
elseif temp == 5
    level = 1;
else
    msg = 'invalid exposure extracted from string';
    error(msg)
end

end
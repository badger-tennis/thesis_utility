function [results, means, stddv] = process_perclos(participants)

numP = length(participants);
baselines = zeros(numP, 4);
means = zeros(numP,1);
stddv = zeros(numP,1);
results = zeros(numP, 8);

for i = 1: numP
    %currently unused
    baselines(i, participants{i}.permutation(1)) = participants{i}.perclos(participants{i}.sync_times(1,1));
    baselines(i, participants{i}.permutation(2)) = participants{i}.perclos(participants{i}.sync_times(21,1));
    baselines(i, participants{i}.permutation(3)) = participants{i}.perclos(participants{i}.sync_times(41,1));
    baselines(i, participants{i}.permutation(4)) = participants{i}.perclos(participants{i}.sync_times(61,1));
    means(i) = mean(participants{i}.perclos);
    stddv(i) = std(participants{i}.perclos);
    perc = participants{i}.perclos;
    perc = perc - means(i);
    perc = perc./stddv(i);
    results(i, participants{i}.permutation(1)) = mean(perc(participants{i}.sync_times(1,1): participants{i}.sync_times(20,2)));
    results(i, participants{i}.permutation(2)) = mean(perc(participants{i}.sync_times(21,1): participants{i}.sync_times(40,2)));
    results(i, participants{i}.permutation(3)) = mean(perc(participants{i}.sync_times(41,1): participants{i}.sync_times(60,2)));
    results(i, participants{i}.permutation(4)) = mean(perc(participants{i}.sync_times(21,1): participants{i}.sync_times(80,2)));
    results(i, (participants{i}.permutation(1)+4)) = mean(participants{i}.perclos(participants{i}.sync_times(1,1): participants{i}.sync_times(20,2)));
    results(i, (participants{i}.permutation(2)+4)) = mean(participants{i}.perclos(participants{i}.sync_times(21,1): participants{i}.sync_times(40,2)));
    results(i, (participants{i}.permutation(3)+4)) = mean(participants{i}.perclos(participants{i}.sync_times(41,1): participants{i}.sync_times(60,2)));
    results(i, (participants{i}.permutation(4)+4)) = mean(participants{i}.perclos(participants{i}.sync_times(21,1): participants{i}.sync_times(80,2)));
    results(i,9) = participants{i}.permutation(1);
    results(i,10) = participants{i}.permutation(2);
    results(i,11) = participants{i}.permutation(3);
    results(i,12) = participants{i}.permutation(4);
end
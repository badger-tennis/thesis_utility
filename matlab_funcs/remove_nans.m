function ret = remove_nans(results)

numP = length(results);
check = isnan(results(:,1));
nanners = find(check);
lenNan = length(nanners);

ret = zeros((numP - lenNan), 12);
count = 1;
for i = 1: numP
    if ~(isnan(results(i,1)))
        ret(count,:) = results(i,:);
        count = count + 1;
    end
end

end
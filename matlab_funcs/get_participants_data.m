function retparticipants = get_participants_data(participants, data)

numParticipants = length(participants);
for i = 1: numParticipants
    participants{i}.perclos = data{i}.perclos;
end

retparticipants = participants;
end
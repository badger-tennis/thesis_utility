function [im_matrix] = amass_data(im_data, test_data)

numParticipants = length(test_data);
numImages = length(im_data);

for i = 1: numImages
    %im_data{i}.frame_num = cell(numParticipants,1);
	%im_data{i}.experiment_time = cell(numParticipants,1);
	im_data{i}.gmt_s = cell(numParticipants,1);
	im_data{i}.gmt_ms = cell(numParticipants,1);
	%im_data{i}.delay = cell(numParticipants,1);
	%im_data{i}.annotation_id = cell(numParticipants,1);
	im_data{i}.track_state = cell(numParticipants,1);
	%im_data{i}.model_qual = cell(numParticipants,1);
	%im_data{i}.right_eye_close = cell(numParticipants,1);
	%im_data{i}.left_eye_close = cell(numParticipants,1);
	%im_data{i}.right_close_conf = cell(numParticipants,1);
	%im_data{i}.left_close_conf = cell(numParticipants,1);
	%im_data{i}.eye_close_calib = cell(numParticipants,1);
	im_data{i}.blinking = cell(numParticipants,1);
	%im_data{i}.blink_freq = cell(numParticipants,1);
	%im_data{i}.blink_duration = cell(numParticipants,1);
	%im_data{i}.perclos = cell(numParticipants,1);
	im_data{i}.gaze_quality_r = cell(numParticipants,1);
	im_data{i}.gaze_quality_l = cell(numParticipants,1);
	im_data{i}.gaze_calibrated = cell(numParticipants,1);
	im_data{i}.saccade = cell(numParticipants,1);
	im_data{i}.gaze_scr_intsect = cell(numParticipants,1);
	im_data{i}.gsi_pixel_x = cell(numParticipants,1);
	im_data{i}.gsi_pixel_y = cell(numParticipants,1);
	im_data{i}.hpos_conf = cell(numParticipants,1);
    im_data{i}.pupil_diameter_l = cell(numParticipants,1);
    im_data{i}.pupil_diameter_r = cell(numParticipants,1);
    for j = 1: numParticipants
        
        
        %im_data{i}.frame_num = test_data{j}.frame_num(im_data{i}.start_times(j):im_data{i}.end_times(j));
		%im_data{i}.experiment_time = test_data{j}.experiment_time(im_data{i}.start_times(j):im_data{i}.end_times(j));
		im_data{i}.gmt_s{j} = test_data{j}.gmt_s(im_data{i}.start_times(j):im_data{i}.end_times(j));
		im_data{i}.gmt_ms{j} = test_data{j}.gmt_ms(im_data{i}.start_times(j):im_data{i}.end_times(j));
		%im_data{i}.delay = test_data{j}.delay(im_data{i}.start_times(j):im_data{i}.end_times(j));
		%im_data{i}.annotation_id = test_data{j}.annotation_id(im_data{i}.start_times(j):im_data{i}.end_times(j));
		im_data{i}.track_state{j} = test_data{j}.track_state(im_data{i}.start_times(j):im_data{i}.end_times(j));
		%im_data{i}.model_qual = test_data{j}.model_qual(im_data{i}.start_times(j):im_data{i}.end_times(j));
		%im_data{i}.right_eye_close = test_data{j}.right_eye_close(im_data{i}.start_times(j):im_data{i}.end_times(j));
		%im_data{i}.left_eye_close = test_data{j}.left_eye_close(im_data{i}.start_times(j):im_data{i}.end_times(j));
		%im_data{i}.right_close_conf = test_data{j}.right_close_conf(im_data{i}.start_times(j):im_data{i}.end_times(j));
		%im_data{i}.left_close_conf = test_data{j}.left_close_conf(im_data{i}.start_times(j):im_data{i}.end_times(j));
		%im_data{i}.eye_close_calib = test_data{j}.eye_close_calib(im_data{i}.start_times(j):im_data{i}.end_times(j));
		im_data{i}.blinking{j} = test_data{j}.blinking(im_data{i}.start_times(j):im_data{i}.end_times(j));
		%im_data{i}.blink_freq = test_data{j}.blink_freq(im_data{i}.start_times(j):im_data{i}.end_times(j));
		%im_data{i}.blink_duration = test_data{j}.blink_duration(im_data{i}.start_times(j):im_data{i}.end_times(j));
		%im_data{i}.perclos = test_data{j}.perclos(im_data{i}.start_times(j):im_data{i}.end_times(j));
		im_data{i}.gaze_quality_r{j} = test_data{j}.gaze_quality_r(im_data{i}.start_times(j):im_data{i}.end_times(j));
		im_data{i}.gaze_quality_l{j} = test_data{j}.gaze_quality_l(im_data{i}.start_times(j):im_data{i}.end_times(j));
		im_data{i}.gaze_calibrated{j} = test_data{j}.gaze_calibrated(im_data{i}.start_times(j):im_data{i}.end_times(j));
		im_data{i}.saccade{j} = test_data{j}.saccade(im_data{i}.start_times(j):im_data{i}.end_times(j));
		im_data{i}.gaze_scr_intsect{j} = test_data{j}.gaze_scr_intsect(im_data{i}.start_times(j):im_data{i}.end_times(j));
		im_data{i}.gsi_pixel_x{j} = test_data{j}.gsi_pixel_x(im_data{i}.start_times(j):im_data{i}.end_times(j));
		im_data{i}.gsi_pixel_y{j} = test_data{j}.gsi_pixel_y(im_data{i}.start_times(j):im_data{i}.end_times(j));
		im_data{i}.hpos_conf{j} = test_data{j}.hpos_conf(im_data{i}.start_times(j):im_data{i}.end_times(j));
        im_data{i}.pupil_diameter_l{j} = test_data{j}.pupil_diameter_l(im_data{i}.start_times(j):im_data{i}.end_times(j));
        im_data{i}.pupil_diameter_r{j} = test_data{j}.pupil_diameter_r(im_data{i}.start_times(j):im_data{i}.end_times(j));
    end
end
im_matrix = im_data;
end
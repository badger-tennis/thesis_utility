function [] = writeParticipantLists(matrix)

matSize = size(matrix);

numP = matSize(2);
numI = matSize(1);
p = cell(numP,1);

for i = 1: numP
    p{i} = sprintf('p%03d.txt', i);
end

formatspec = '%s\n';

for j = 1: numP
    path = strcat('D:\TIM\Thesis\utility\image_lists\', p{j});
    fid = fopen(path, 'w');
    %%fprintf(fid, formatspecStart, 'gray.pfm');
    for i = 1: numI
        fprintf(fid, formatspec, matrix{i,j});
    end
    fclose(fid);
end
end
function [list] = imageListGen(path, maxImages)

cd(path);

files = dir('*.pfm');

len = length(files);

if len < maxImages
    msg = 'Not enough images';
    error(msg);
end

names = cell(len, 1);
for i = 1: len
    names{i} = files(i).name;
end

inds = randperm(len);

list = cell(maxImages,1);
for i = 1: maxImages
    list{i} = names{inds(i)};
end



% for i = 1: (len - maxImages)
%     clear names{inds(i)};
% end



end
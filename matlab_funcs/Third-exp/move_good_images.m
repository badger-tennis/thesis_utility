function [] = move_good_images(list)

len = length(list);

for i = 1: len
    name500 = strcat('500/', strrep(list(i).name, '_10000.pfm', ''), '_500.pfm');
    name2000 = strcat('2000/', strrep(list(i).name, '_10000.pfm', ''), '_2000.pfm');
    name4000 = strcat('4000/', strrep(list(i).name, '_10000.pfm', ''), '_4000.pfm');
    name8000 = strcat('8000/', strrep(list(i).name, '_10000.pfm', ''), '_8000.pfm');
    
    movefile(name500, 'trash');
    movefile(name2000, 'trash');
    movefile(name4000, 'trash');
    movefile(name8000, 'trash');
end
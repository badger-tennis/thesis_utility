function [hdrInfo, exrInfo, pfmInfo, hdrResolution, exrResolution, pfmResolution, hdrMinMax, exrMinMax, pfmMinMax] = analyseImageDirectory(path)

cd(path);
pfmInfo = dir('*.pfm');
hdrInfo = dir('*.hdr');
exrInfo = dir('*.exr');


pfmLen = length(pfmInfo);
hdrLen = length(hdrInfo);
exrLen = length(exrInfo);

% 1 - resolution
% 2 - peak brightness
% 3 - min brightness
% 4 - 

exrResolution = zeros(exrLen, 2);
exrMinMax = zeros(exrLen, 2);
if (exrLen ~= 0)
    for i = 1: exrLen
        temp = exrread(exrInfo(i).name);
        tempSize = size(temp);
        exrResolution(i,1) = tempSize(1);
        exrResolution(i,2) = tempSize(2);
        exrMinMax(i,1) = min(min(min(temp)));
        exrMinMax(i,2) = max(max(max(temp)));
        clear temp;
    end
end

hdrResolution = zeros(hdrLen, 2);
hdrMinMax = zeros(hdrLen, 2);
if (hdrLen ~= 0)
    for i = 1: hdrLen
        temp = hdrread(hdrInfo(i).name);
        tempSize = size(temp);
        hdrResolution(i,1) = tempSize(1);
        hdrResolution(i,2) = tempSize(2);
        hdrMinMax(i,1) = min(min(min(temp)));
        hdrMinMax(i,2) = max(max(max(temp)));
        clear temp;
    end
end

pfmResolution = zeros(pfmLen, 2);
pfmMinMax = zeros(pfmLen, 2);
if (pfmLen ~= 0)
    for i = 1: pfmLen
        temp = pfmread(pfmInfo(i).name);
        tempSize = size(temp);
        pfmResolution(i,1) = tempSize(1);
        pfmResolution(i,2) = tempSize(2);
        pfmMinMax(i,1) = min(min(min(temp)));
        pfmMinMax(i,2) = max(max(max(temp)));
        clear temp;
    end
end

end
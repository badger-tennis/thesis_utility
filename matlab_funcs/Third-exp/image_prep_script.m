
if ~(ismember('hdrInfo', who))
    [hdrInfo, exrInfo, pfmInfo, hdrResolution, exrResolution, ...
        pfmResolution, hdrMinMax, exrMinMax, pfmMinMax] = analyseImageDirectory(path);
end

checkEXR = find((exrResolution(:,1) >= 1080) & (exrResolution(:,2) >= 1920));
checkHDR = find((hdrResolution(:,1) >= 1080) & (hdrResolution(:,2) >= 1920));
checkPFM = find((pfmResolution(:,1) >= 1080) & (pfmResolution(:,2) >= 1920));
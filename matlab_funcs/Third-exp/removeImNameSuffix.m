function [list] = removeImNameSuffix(unclean)

len = length(unclean);

list = cell(len, 1);
for i = 1: len
    list{i} = strrep(unclean{i}, '_10000', '');
end
end
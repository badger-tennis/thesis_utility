%this was explicitly written for the experiment I conducted in portugal,
%it's not very adaptable to different numbers. I imagine that no one will
%ever read this though, so eh, fuck it.

function [nameMatrix] = genParticipantLists(list)

len = length(list);
initNum = 72;
adapt = 24;


nameMatrix = cell(len, (initNum+adapt));
for i = 1: (initNum)
    inds = randperm(len);
    if (rem(i, 3) == 1)
        suff = '_500.pfm';
        pref = '500/';
    elseif (rem(i, 3) == 2)
        suff = '_4000.pfm';
        pref = '4000/';
    else
        suff = '_8000.pfm';
        pref = '8000/';
    end
    for j = 1: len
        nameMatrix{j, i} = strcat(pref, strrep(list{inds(j)}, '.pfm', ''), suff);
    end
end

start = initNum + 1;
finish = initNum + adapt;

for i = start : finish
    inds = randperm(len);
    suff = '_2000.pfm';
    pref = '2000/';
    for j = 1: len
        nameMatrix{j, i} = strcat(pref, strrep(list{inds(j)}, '.pfm', ''), suff);
    end
end
    
end


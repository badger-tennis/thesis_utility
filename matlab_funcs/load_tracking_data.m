function [raw_data, field_list] = load_tracking_data(parent)
results_directories = load_dirs(parent);
%add any extra fields you want to the field list. most of the terms are
%self explanatory.

field_list = {'frame_num','experiment_time','gmt_s','gmt_ms','delay', ...
    'annotation_id','track_state','model_qual','right_eye_close', ...
    'left_eye_close','right_close_conf','left_close_conf', ...
    'eye_close_calib','blinking','blink_freq','blink_duration', ...
    'perclos','gaze_quality_r','gaze_quality_l','gaze_calibrated', ...
    'saccade','gaze_scr_intsect','gsi_pixel_x','gsi_pixel_y', 'hpos_conf',... 
    'pupil_diameter_l', 'pupil_diameter_r'};

numParticipants = length(results_directories);
raw_data = cell(numParticipants, 1);

for i = 1: numParticipants
    [raw_data{i}, ~] = fll_read_filepath(results_directories{i}, field_list);
end

end
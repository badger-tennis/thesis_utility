%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------Process Tracking Data Script--------------------------%
%--------------------tim.bradley.256@gmail.com----------------------------%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tic
%TERMINATE PATHS WITH '\'
parent = 'D:\Google Drive\eye_results\';
%parent = 'C:\Users\bradle_t\Google Drive\eye_results\';
logpath = 'D:\git_projects\thesis_utility\matlab_funcs\eye_logs\';
%logpath = 'C:\Users\bradle_t\Documents\thesis\utility\matlab_funcs\eye_logs\';
matlab_path = 'D:\Documents\MATLAB';
%matlab_path = 'C:\Users\bradle_t\Documents\MATLAB';
mit_path = 'D:\Documents\MATLAB\mit_saliency_code';
%mit_path = 'C:\Users\bradle_t\Documents\MATLAB\mit_saliency';

seconds = 0;

%load output of experiment program
formatspec = '%s %u64 %u64';
if ~(ismember('eye_logs', who))
    eye_logs = load_eye_logs(logpath, formatspec);
end

clear formatspec

%load facelab/eye-tracking data
if ~(ismember('data', who))
    [data, field_list] = load_tracking_data(parent);
    data = discard_unused_data(data, eye_logs);
end
numParticipants = length(data);

%data = load_tracking_data(parent);



%find list of image names, removed folder and extension. My folder names
%(containing images) were all numbers, if you only use letters change
%the the regexp bit in declutter_filenames.m from \d to \s.
imageNames = get_image_names(eye_logs{1,1}, 1);

%Intialises array of ImageData class objects with names of images used in
%experiment and other relevant data
im_data = initialise_im_data(eye_logs, imageNames);

%I had to leave '/' in in decluter_filename() because one of my image's
%names was a substring of another, so I removed it here.    
%imageNames = annoying_fix(imageNames);

%likely irrelevant to your experiment
im_data = get_exposure_levels(im_data, eye_logs);

%participants class may need to be adjusted for different experiments
participants = initialise_participants(numParticipants);

%this is specific to my experiment, so can be commented out
participants = get_exposure_order(participants,eye_logs);

%this function collects the perclos data for each participant it was meant
%to include more fields, however other functions now collect those
participants = get_participants_data(participants, data);

%This gets the indexes for each image and includes that data into the
%participant objects, along with the order the images are displayed in
[sync_times, participants] = sync_images(data, eye_logs, participants);

clear eye_logs
% This function is very important, it gets the start and end indices for
% each image across all participants
im_data = get_start_end(im_data, sync_times);

im_data = amass_data(im_data, data);


gauss_window = 118;
x_res = 1920;
y_res = 1080;
if ~(ismember('fixMaps', who))
    fixMaps = cell(length(im_data),1);
    for i = 1: length(im_data)        
        fixMaps{i} = fixation_density_maps(im_data{i}, gauss_window, x_res, y_res, seconds); %seconds is a cutoff 0 is default
    end
end


if ~(ismember('fdms', who))
    fdms = cell(length(im_data),4);
    for i = 1: length(im_data)
        for j = 1: 4
            [fd_t, ~] = run_antonioGaussian(fixMaps{i}{j}, (gauss_window/2));
            fd_t(fd_t<0) = 0;
            fdms{i, j} = fd_t;
        end
    end
end

% k =4;
% tic
% %not sure whether brightness one needs to be included in inter-comparisons
% if ~(ismember('k_res', who))
%     k_res = 1;
%     kemd = cell(length(im_data),1);
%     ksim = cell(length(im_data),1);
%     kroc = cell(length(im_data),1);
%     kccs = cell(length(im_data),1);
%     knss = cell(length(im_data),1);
%     kkld = cell(length(im_data),1);
%     interemd = cell(length(im_data),1);
%     intersim = cell(length(im_data),1);
%     interroc = cell(length(im_data),1);
%     interccs = cell(length(im_data),1);
%     internss = cell(length(im_data),1);
%     interkld = cell(length(im_data),1);
%     for i = 1: length(im_data)
%          [kemd{i}, ksim{i}, kroc{i}, kccs{i}, knss{i}, kkld{i}, interemd{i}, intersim{i}, interroc{i}, interccs{i}, internss{i}, interkld{i}] = ...
%              k_fold_res(k, im_data{i}, gauss_window, x_res, y_res, seconds);
%     end
% end
% toc

% 
% fuckbeans = 0;
% if ~(ismember('kld_results', who))
%     kld_results = zeros(length(im_data),4);
%     for i = 1: length(im_data)
%         for j = 1:4
%             kl_tst = KLdiv(fdms{i, 1}, fdms{i, j});
%             kl_test2 = KLdiv(fdms{i, j}, fdms{i, 1});
%             if ~isreal(kl_tst)
%                 fuckbeans = fuckbeans + 1;
%             end
%             kld_results(i,j) = (kl_tst + kl_test2)/2;
%         end
%     end
% end

% tic
% if ~(ismember('emd_results', who))
%     emd_results = zeros(length(im_data), 4);
%     for i = 1: length(im_data)
%         for j = 1:4
%             emd_results(i, j) = EMD(fdms{i, 1}, fdms{i, j}, 0, 64);
%         end
%     end
% end
% toc
% 
% if ~(ismember('sim_results', who))
%     sim_results = zeros(length(im_data),4);
%     for i = 1: length(im_data)
%         for j = 1:4
%             sim_results(i,j) = similarity(fdms{i, 1}, fdms{i, j}, 0);
%         end
%     end
% end
% 
% if ~(ismember('roc_results', who))
%     roc_results = zeros(length(im_data),4);
%     for i = 1: length(im_data)
%         for j = 1:4
%             [roc_results(i,j), ~,~,~] = AUC_Judd(fdms{i, 1}, fixMaps{i}{j}, 1, 0);
%         end
%     end
% end
% 
% if ~(ismember('cc_results', who))
%     cc_results = zeros(length(im_data),4);
%     for i = 1: length(im_data)
%         for j = 1:4
%             cc_results(i,j) = CC(fdms{i, 1}, fdms{i, j}); 
%         end
%     end
% end
% 
% if ~(ismember('nss_results', who))
%     nss_results = zeros(length(im_data),4);
%     for i = 1: length(im_data)
%         for j = 1:4
%             nss_results(i,j) = NSS(fdms{i, 1}, fixMaps{i}{j});
%         end
%     end
% end
% % 
% if ~(ismember('results', who))
%     [results, means, stddv] = process_perclos(participants);
% end
% %presentation shit, delete
% 
% % lengths of amassed data
% lengths = zeros(length(data),1);
% for i = 1: numParticipants
%     lengths(i) = length(data{i}.perclos);
% end
% 
% %test pupil data
% pupil_l = zeros(length(data),1);
% pupil_r = zeros(length(data),1);
% for i = 1: length(data)
%     pupil_l(i) = mean(data{i}.pupil_diameter_l);
%     pupil_r(i) = mean(data{i}.pupil_diameter_r);
% end
% 


toc

% I need to:

% 1.  Concatenate the times for the tracking data
% 2.  Sync the tracking data with the eye data
%    2.1  Find universal start and end times for each participant
% 3.  Gather the same images in a different structure
%    3.1  Store which exposure each image is
%    3.2  Store the position in the order as well
% 4.  Create perclos structure for each participant
%    4.1  Store brightness and order
% 5.  
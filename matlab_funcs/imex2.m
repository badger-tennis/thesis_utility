function [ f ] = imex2( img, varargin ) % 
%IMEX2 imshow but with a mouse wheel exposure setting
%   Title is optional
%   Version 2
%   By Jon Hatchett

%     if ~exist('fig', 'var')
%         fig = figure();
%     end
%     
%     if ~exist('callback', 'var')
%         callback = @(x)x;
%     end

    p = inputParser;
    p.addRequired('img', @isnumeric);
    p.addOptional('title', '', @ischar);
    p.addParameter('figure', 0);
    p.addParameter('callback', 0);
    p.addParameter('useGPU', 1);
    p.parse(img, varargin{:});
    
    title = p.Results.title;
    fig = p.Results.figure;
    if fig == 0
        fig = figure();
    end
    callback = p.Results.callback;
    if callback == 0
        callback = @(x)x;
    end
    
    set(fig, 'Color', [0 0 0]);
    set(fig, 'WindowScrollWheelFcn', @scroll);
    set(fig, 'KeyPressFcn', @keypress);
    set(fig, 'menubar', 'none');
    
    [height, width, ~] = size(img);

    if p.Results.useGPU
       %img = gpuArray(img);
    end
    
    value = 0;
    scale = 1;
    
    setTitle();
    
    set(fig, 'SizeChangedFcn', @resize);
    
    show(value);
    
    function show(val)
        if (exist('im', 'var'))
            imclose(im);
        end
        warning('off', 'images:initSize:adjustingMag');
        im = imshow(callback(abs(power(complex(double(img) ./ (2 .^ -val)), complex(1/2.2, 0)))), 'parent', gca, 'Border', 'tight'); 
        %if isa(callback, 'function_handle')
        %    im = imshow(real(power(complex(double(img) ./ (2 .^ -val)), complex(1/2.2, 0))), 'parent', gca, 'Border', 'tight'); 
        %else
        %    i = gather(real(power(complex(double(img) ./ (2 .^ -val)), complex(1/2.2, 0))));
        %    im = imshow(callback(i), 'parent', gca, 'Border', 'tight'); 
        %end
        warning('on', 'images:initSize:adjustingMag');
    end

    function setTitle()
        if ~strcmp(title, '')
            fig.Name = sprintf('%s: %.2fev %.0f%%', title, value, scale * 100);
            fig.NumberTitle = 'off';
        else
            fig.Name = sprintf('%.2fev %.0f%%', value, scale * 100);
            fig.NumberTitle = 'on';
        end
    end

    function scroll(source, callbackdata)
        value = min(max(value - callbackdata.VerticalScrollCount, -20), 20);
        show(value);
        setTitle();
    end

    function resize(source, callbackdata)
        pos = get(fig, 'Position');
        scale = pos(3) / width;
        setTitle();
        %display(sprintf('Image displayed at %.0f%%', (pos(3) / width) * 100));
        show(value);
    end

    function keypress(source, callbackdata)
        char = get(fig,'CurrentCharacter');
        switch char
            case 8
                value = 0;
                show(value);
                setTitle();
            case 27
                close(fig);
            case 'o'
                pos = get(fig, 'Position');
                set(fig, 'Position', [pos(1) pos(2) width height]);
        end
    end

    if nargout > 0 % Detect whether the return value will be used.
        f = fig;
    end

end

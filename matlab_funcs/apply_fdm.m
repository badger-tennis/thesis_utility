function [im] = apply_fdm(pfm, fdm)

peak = max(max(max(pfm)));
im = ReinhardTMO(pfm, 0.5, peak);
im(:,:,1) = im(:,:,1).*fdm;
im(:,:,2) = im(:,:,2).*fdm;
im(:,:,3) = im(:,:,3).*fdm;

end
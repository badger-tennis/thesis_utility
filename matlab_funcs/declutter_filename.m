function [rep_name] = declutter_filename(filename)

temp = regexprep(filename, '_\d*.pfm', '');
rep_name = regexprep(temp, '\d*/', '/');

end
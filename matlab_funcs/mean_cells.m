function [res] = mean_cells(in)

len = length(in);
width = size(in{1});
width = width(2);
res = zeros(len, width);

for i = 1: len
    res(i, :) = mean(in{i});
end
function [dir_paths] = load_dirs(parent_directory)

d = dir(parent_directory);
isub = [d(:).isdir]; %# returns logical vector
dir_names = {d(isub).name}';
dir_names(ismember(dir_names,{'.','..'})) = [];

dir_paths = strcat(parent_directory, dir_names, '\');
end
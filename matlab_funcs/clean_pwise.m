function [newret, x] = clean_pwise(ret)

dim = size(ret);
checkret = ret(1,:);
numP = dim(2)/3;
nanners = isnan(checkret);
stupidInd = find(nanners);
newSize = 3*(numP - length(stupidInd));
thirdnewSize = newSize/3;

newret = zeros(dim(1), newSize);

x = 1;
count = 1;
legit = 1;
while (count <= numP)
%     if (count == 22)
%         count;
%     end
    if ~(nanners(count))
        newret(:, legit) = ret(:, count);
        newret(:, (thirdnewSize + legit)) = ret(:, (numP + count));
        newret(:, ((2*thirdnewSize) + legit)) = ret(:, ((2*numP) + count));
        legit = legit + 1;
    else 
        x = x + 1;
    end

    count = count + 1;
end
end
function [matrix] = create_matrix_80(numParticipants)



permlist_temp = perms(1:4);
count = 1;
permlist = zeros(20,4);
matrix = zeros(80,numParticipants);
for i = 2: 23
    if i == 8 || i == 17
        continue;
    else
        permlist(count,:) = permlist_temp(i,:);
        count = count + 1;
    end
end

permlist = [permlist;permlist;permlist;permlist];

for j = 0 : ((numParticipants/4) - 1)
    index = randperm(80);
    for k = 1: 80
        matrix(k, j*4 + 1) = permlist(index(k), 1);
        matrix(k, j*4 + 2) = permlist(index(k), 2);
        matrix(k, j*4 + 3) = permlist(index(k), 3);
        matrix(k, j*4 + 4) = permlist(index(k), 4);
    end
end


end
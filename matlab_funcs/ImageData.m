classdef ImageData
    properties
        name
        start_times
        end_times
        exposure_levels
        indices
        track_state
        %right_eye_close
        %left_eye_close
        %right_close_conf
        %left_close_conf
        %eye_close_calib
        gmt_s
        gmt_ms
        blinking
        gaze_quality_r
        gaze_quality_l
        gaze_calibrated
        saccade
        gaze_scr_intsect
        gsi_pixel_x
        gsi_pixel_y
        hpos_conf
        pupil_diameter_l
        pupil_diameter_r
        categoryLeft
        categoryRight
        ambient_levels
        fix_bool
    end
    methods
        
        function obj = set.name(obj, filename)
            if ischar(filename)
                obj.name = filename;
            else
                msg = 'filename must be string';
                error(msg)
            end
        end
        
    end
end
function [image_data] = initialise_im_data(eye_logs, image_list)

%this function takes the logs from the experiment program and the list of
%images and finds the position in the order which each image was shown to
%every participant. It also initialises an array of ImageData objects

x = size(eye_logs);
numParticipants = x(1);
numImages = length(image_list);
indices = zeros(numParticipants, 1);
image_data = cell(numImages,1);
for i = 1: numImages
    image_data{i} = ImageData;
    image_data{i}.name = image_list{i};
    image_data{i}.indices = indices;
    for j = 1: numParticipants
        IndexC = strfind(eye_logs{j,1}, image_list{i});
        index = find(not(cellfun('isempty', IndexC)));
        image_data{i}.indices(j) = index;
    end
end


end
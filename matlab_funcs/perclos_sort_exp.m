function [ret] = perclos_sort_exp(results, concern)

temp = size(results);
numP = temp(1);
ret = zeros(numP,4);

%concern = 1;

concerncheck1 = (results(:,9) == concern);
concernPos1 = find(concerncheck1);
conLen1 = length(concernPos1);
concerncheck2 = (results(:,10) == concern);
concernPos2 = find(concerncheck2);
conLen2 = length(concernPos2);
concerncheck3 = (results(:,11) == concern);
concernPos3 = find(concerncheck3);
conLen3 = length(concernPos3);
concerncheck4 = (results(:,12) == concern);
concernPos4 = find(concerncheck4);
conLen4 = length(concernPos4);

start = 0;
for i = 1: conLen1
    ret(i,1) = results(concernPos1(i), concern);
    ret(i,2) = results(concernPos1(i), (concern + 4));
    ret(i, 3) = concern;
    ret(i, 4) = 1;
    
end

start = start + conLen1;
for i = 1: conLen2
    ret((start+i),1) = results(concernPos2(i), concern);
    ret((start+i),2) = results(concernPos2(i), (concern + 4));
    ret((start+i), 3) = concern;
    ret((start+i), 4) = 2;
    
end

start = start + conLen2;
for i = 1: conLen3
    ret((start+i),1) = results(concernPos3(i), concern);
    ret((start+i),2) = results(concernPos3(i), (concern + 4));
    ret((start+i), 3) = concern;
    ret((start+i), 4) = 3;
    
end

start = start + conLen3;
for i = 1: conLen4
    ret((start+i),1) = results(concernPos4(i), concern);
    ret((start+i),2) = results(concernPos4(i), (concern + 4));
    ret((start+i), 3) = concern;
    ret((start+i), 4) = 4;
    
end
end
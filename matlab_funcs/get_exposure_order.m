function [retparticipants] = get_exposure_order(participants, eye_logs)

numParticipants = length(participants);
doublecheck = size(eye_logs);

if (numParticipants ~= doublecheck(1))
    msg = 'participant number does not match eye_logs';
    error(msg)
end

for i = 1 : numParticipants
    participants{i}.permutation(1) = parse_exposure(eye_logs{i,1}{1}(1));
    participants{i}.permutation(2) = parse_exposure(eye_logs{i,1}{21}(1));
    participants{i}.permutation(3) = parse_exposure(eye_logs{i,1}{41}(1));
    participants{i}.permutation(4) = parse_exposure(eye_logs{i,1}{61}(1));
end

retparticipants = participants;

end
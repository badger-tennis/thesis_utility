function [ret] = perclos_p_wise(im_data, participants)
    numI = length(im_data);
    numP = length(participants);
    ret = zeros(numI, 6);
    means = zeros(numP, 1);
    stddv = zeros(numP, 1);
    percret = zeros(numI, numP);
    indret = zeros(numI, numP);
    permret = zeros(numI, numP);
    
    for i = 1: numP
        means(i) = mean(participants{i}.perclos);
        stddv(i) = std(participants{i}.perclos);
        perc = participants{i}.perclos;
        perc = perc - means(i);
        perc = perc./stddv(i);
        
        for j = 1: numI
            percret(j, i) = perc(im_data{j}.indices(i,1):im_data{j}.indices(i,2));
            indret(j,i) = im_data{j}.indices(i);
            if (im_data{j}.indices <= 20)
                permret(j,i) = participants{i}.permutation(1);
            elseif (20 < im_data{j}.indicies(i) <= 40)
                permret(j,i) = participants{i}.permutation(2);
            elseif (40 < im_data{j}.indicies(i) <= 60)
                permret(j,i) = participants{i}.permutation(3);
            elseif (60 < im_data{j}.indicies(i) <= 80)
                permret(j,i) = participants{i}.permutation(3);
            end
        end
    end
ret = [percret, indret, permret];
end

% for i = 1: 80
%     c(b(i), :) = randperm(4);
% end
% 
% i = 1;
% while i ~= 80
%    if c(b(i), counter(b(i))) == 1 && j < 20
%        j = j + 1;
%        i = i + 1;
%    elseif c(b(i),counter(b(i))) == 2 && k < 20
%        k = k + 1;
%        i = i + 1;
%    elseif c(b(i), counter(b(i))) == 3 && l < 20
%        l = l+1;
%        i = i + 1;
%    elseif c(b(i), counter(b(i))) == 4 && m < 20
%        m = m + 1;
%        i = i + 1;
%    else
%        for f = (80 - i): 80
%           c(b(f), :)  = randperm(4);
%           
%        end
%        counter(b(i)) = counter(b(i)) - 1;
%    end           
%    counter(b(i)) =  counter(b(i)) + 1;
% end

%   1. n groups

%   2. m images

%   3. p participants

%   4. m % n == 0 

%   5. each image, i, is in each of n groups before it is placed in the
%   same group twice

%   6. total images assigned (ti) = m * p

%   7. images per group (gi) = m/n

%   8. p = 2*n!

%   9. ti = m * 2 * n!

%   10. try counting each element to 12 for each group

%   assign each picture to all groups necessary, randomise order and
%   participant
                        
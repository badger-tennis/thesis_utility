function [mean_start, mean_end, mean_diff, mean_exp_diff] = perclos_presentation(participants)

numP = length(participants);
startperc = zeros(numP, 4);
endperc = zeros(numP, 4);
exp_start = zeros(numP, 1);
exp_end = zeros(numP, 1);

for i = 1: numP
    startperc(i, participants{i}.permutation(1)) = participants{i}.perclos(participants{i}.sync_times(1));
    endperc(i, participants{i}.permutation(1)) = participants{i}.perclos(participants{i}.sync_times(20)+600);
    
    startperc(i, participants{i}.permutation(2)) = participants{i}.perclos(participants{i}.sync_times(21));
    endperc(i, participants{i}.permutation(2)) = participants{i}.perclos(participants{i}.sync_times(40)+600);
    
    startperc(i, participants{i}.permutation(3)) = participants{i}.perclos(participants{i}.sync_times(41));
    endperc(i, participants{i}.permutation(3)) = participants{i}.perclos(participants{i}.sync_times(60)+600);
    
    startperc(i, participants{i}.permutation(4)) = participants{i}.perclos(participants{i}.sync_times(61));
    endperc(i, participants{i}.permutation(4)) = participants{i}.perclos(participants{i}.sync_times(80)+600);
    
    exp_start(i) = participants{i}.perclos(participants{i}.sync_times(1));
    exp_end(i)  = participants{i}.perclos(participants{i}.sync_times(80)+600);
end

mean_start = mean(startperc);
mean_end = mean(endperc);

mean_diff = mean( (endperc-startperc));

mean_exp_diff = mean((exp_end - exp_start));


end
function fix = temp_fix_inter_results(input)

numIm = length(input);

%knum = size(input{1});
fix = zeros(numIm, 3);

for i = 1:numIm
    for j = 1:3
        fix(i, j) = mean(mean(input{i}{j,1}));
    end
end

end
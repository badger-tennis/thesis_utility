function [im_matrix] = get_start_end(im_data, sync_times)

x = size(im_data);
numImages = x(1);
y = size(sync_times);
numParticipants = y(1);
im_matrix = im_data;
for i = 1: numImages
    im_matrix{i}.start_times = zeros(numParticipants, 1);
    im_matrix{i}.end_times = zeros(numParticipants, 1);
     for j = 1: numParticipants
         im_matrix{i}.start_times(j) = sync_times{j}(im_matrix{i}.indices(j),1);
         im_matrix{i}.end_times(j) = sync_times{j}(im_matrix{i}.indices(j),2);
     end
end


end
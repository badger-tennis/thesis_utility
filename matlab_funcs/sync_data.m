function [sync_start, sync_end] = sync_data(test_data, eye_log)

% ind = 3;
% eye_log{1} = eye_logs{ind, 1};
% eye_log{2} = eye_logs{ind, 2};
% eye_log{3} = eye_logs{ind, 3};
% 
% test_data = data{ind};

%ret = cell(size(eye_log));
x = size(test_data);
num_participants = x(1);
numImages = length(eye_log{1,1});
data_entries = zeros(num_participants,1);
log_entries = zeros(num_participants,1);
sync_start = zeros(num_participants,1);
sync_end = zeros(num_participants,1);
%under = false(num_participants,1);
%diff = zeros(num_participants,1);
%end_under = false(num_participants,1);
%end_diff = zeros(num_participants,1);


for j = 1: num_participants
    data_entries(j) = length(test_data{j}.gmt_s);
    log_entries(j) = length(eye_log{j,1});
    ptime = zeros(data_entries(j),1);

    for i = 1: data_entries(j)
        ptime(i) = (test_data{j}.gmt_s(i)*1000 + test_data{j}.gmt_ms(i));
    end

    count = 1;
    while ptime(count) < eye_log{j,2}(1)
        count = count + 1;
    end
    
    end_count = 1;
    while ptime(end_count) < eye_log{j,3}(numImages)
        end_count = end_count + 1;
    end
    
    count = count - 1;
    %end_count = end_count - 1;

%     over_diff = ptime(count) - eye_log{j,2}(1);
%     under_diff = eye_log{j,2}(1) - ptime(count-1);
% 
%     %under = 0;
%     if under_diff < over_diff
%         count = count -1;
%         under(j) = 1;
%         diff(j) = eye_log{j,2}(1) - ptime(count);
%     else 
%         diff(j) = ptime(count) - eye_log{j,2}(1);
%     end
%     
%     over_diff = ptime(end_count) - eye_log{j,3}(numImages);
%     under_diff = eye_log{j,3}(numImages) - ptime(end_count-1);
% 
%     %under = 0;
%     if under_diff < over_diff
%         count = count -1;
%         end_under(j) = 1;
%         end_diff(j) = eye_log{j,3}(numImages) - ptime(end_count);
%     else 
%         end_diff(j) = ptime(count) - eye_log{j,3}(numImages);
%     end

    sync_start(j) = count;
    sync_end(j) = end_count;

end

end
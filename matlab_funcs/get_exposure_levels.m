function [im_data] = get_exposure_levels(im_data, eye_logs)

numImages = length(eye_logs{1,1});
x = size(eye_logs);
numParticipants = x(1);


for i = 1: numImages
    im_data{i}.exposure_levels = zeros(numParticipants,1);
    for j = 1: numParticipants
        temp = str2double(eye_logs{j,1}{im_data{i}.indices(j)}(1));
        if temp == 1
            im_data{i}.exposure_levels(j) = 2;
        elseif temp == 2
            im_data{i}.exposure_levels(j) = 3;
        elseif temp == 4
            im_data{i}.exposure_levels(j) = 4;
        elseif temp == 5
            im_data{i}.exposure_levels(j) = 1;
        else
            msg = 'invalid exposure extracted from string';
            error(msg)
        end
    end
end

end
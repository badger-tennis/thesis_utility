function [ret] = get_image_names(filenames, sorted)

numFiles = length(filenames);
temp = cell(numFiles, 1);

for i = 1: numFiles
    temp{i} = declutter_filename(filenames{i});
end

if (sorted == 1)
    ret = sort(temp);
else
    ret = temp;
end
end
function [file_list] = check_contents_of_directories(directoryList)

numDirs = length(directoryList);
cd(directoryList{1});
filestemp = dir('*.pfm');
numberOfScenes = length(filestemp);
file_list = cell(numberOfScenes,numDirs);
cd ..;

b = zeros(numDirs, 1);

for i = 1 : numDirs
    cd(directoryList{i});
    file_list_holder_temp = dir('*.pfm');
    b(i) = length(file_list_holder_temp);
    cd ..;
end

if range(b) ~= 0
   msg = 'directories have different numbers of pfm files'; 
   error(msg);
end

for i = 1 : numDirs
    cd(directoryList{i});
    file_list_holder = dir('*.pfm');
    for j = 1 : numberOfScenes
       file_list{j,i} = file_list_holder(j).name; 
    end
    cd ..;
end


end
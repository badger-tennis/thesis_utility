function [blinkrate] = getBlinkRate(blink_bool, sync_times_cell)

blinkrate = zeros(1,4);
sync = sync_times_cell;
blinkrate(1) = ceil((length(find(logical(diff(blink_bool(sync(1,1):sync(15,2)))))))/2);
blinkrate(2) = ceil((length(find(logical(diff(blink_bool(sync(16,1):sync(30,2)))))))/2);
blinkrate(3) = ceil((length(find(logical(diff(blink_bool(sync(31,1):sync(45,2)))))))/2);
blinkrate(4) = ceil((length(find(logical(diff(blink_bool(sync(46,1):sync(60,2)))))))/2);

end
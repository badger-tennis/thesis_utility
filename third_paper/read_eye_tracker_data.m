function [data, names] = read_eye_tracker_data(directory, numParticipants)

origin = cd;
cd(directory);

data = cell(numParticipants, 1);
filenames = dir('*.csv');

for i = 1: numParticipants
    data{i} = iViewX_csvReadAsTable(filenames(i).name);
end
names = filenames;
cd(origin);
end
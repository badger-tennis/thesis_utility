function participantdetails = importParticipantInfo(filename, startRow, endRow)
%IMPORTFILE Import numeric data from a text file as a matrix.
%   PARTICIPANTDETAILS = IMPORTFILE(FILENAME) Reads data from text file
%   FILENAME for the default selection.
%
%   PARTICIPANTDETAILS = IMPORTFILE(FILENAME, STARTROW, ENDROW) Reads data
%   from rows STARTROW through ENDROW of text file FILENAME.
%
% Example:
%   participantdetails = importfile('participant_details.csv', 2, 49);
%
%    See also TEXTSCAN.

% Auto-generated by MATLAB on 2017/09/05 17:45:53

%% Initialize variables.
delimiter = ',';
if nargin<=2
    startRow = 2;
    endRow = inf;
end

%% Read columns of data as text:
% For more information, see the TEXTSCAN documentation.
formatSpec = '%s%s%s%s%s%s%s%s%s%s%s%s%s%s%[^\n\r]';

%% Open the text file.
fileID = fopen(filename,'r','n','UTF-8');
% Skip the BOM (Byte Order Mark).
fseek(fileID, 3, 'bof');

%% Read columns of data according to the format.
% This call is based on the structure of the file used to generate this
% code. If an error occurs for a different file, try regenerating the code
% from the Import Tool.
dataArray = textscan(fileID, formatSpec, endRow(1)-startRow(1)+1, 'Delimiter', delimiter, 'TextType', 'string', 'HeaderLines', startRow(1)-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');
for block=2:length(startRow)
    frewind(fileID);
    dataArrayBlock = textscan(fileID, formatSpec, endRow(block)-startRow(block)+1, 'Delimiter', delimiter, 'TextType', 'string', 'HeaderLines', startRow(block)-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');
    for col=1:length(dataArray)
        dataArray{col} = [dataArray{col};dataArrayBlock{col}];
    end
end

%% Close the text file.
fclose(fileID);

%% Convert the contents of columns containing numeric text to numbers.
% Replace non-numeric text with NaN.
raw = repmat({''},length(dataArray{1}),length(dataArray)-1);
for col=1:length(dataArray)-1
    raw(1:length(dataArray{col}),col) = mat2cell(dataArray{col}, ones(length(dataArray{col}), 1));
end
numericData = NaN(size(dataArray{1},1),size(dataArray,2));

for col=[1,2,7,8,9,10,11,12,13,14]
    % Converts text in the input cell array to numbers. Replaced non-numeric
    % text with NaN.
    rawData = dataArray{col};
    for row=1:size(rawData, 1)
        % Create a regular expression to detect and remove non-numeric prefixes and
        % suffixes.
        regexstr = '(?<prefix>.*?)(?<numbers>([-]*(\d+[\,]*)+[\.]{0,1}\d*[eEdD]{0,1}[-+]*\d*[i]{0,1})|([-]*(\d+[\,]*)*[\.]{1,1}\d+[eEdD]{0,1}[-+]*\d*[i]{0,1}))(?<suffix>.*)';
        try
            result = regexp(rawData(row), regexstr, 'names');
            numbers = result.numbers;
            
            % Detected commas in non-thousand locations.
            invalidThousandsSeparator = false;
            if numbers.contains(',')
                thousandsRegExp = '^\d+?(\,\d{3})*\.{0,1}\d*$';
                if isempty(regexp(numbers, thousandsRegExp, 'once'))
                    numbers = NaN;
                    invalidThousandsSeparator = true;
                end
            end
            % Convert numeric text to numbers.
            if ~invalidThousandsSeparator
                numbers = textscan(char(strrep(numbers, ',', '')), '%f');
                numericData(row, col) = numbers{1};
                raw{row, col} = numbers{1};
            end
        catch
            raw{row, col} = rawData{row};
        end
    end
end


%% Split data into numeric and string columns.
rawNumericColumns = raw(:, [1,2,7,8,9,10,11,12,13,14]);
rawStringColumns = string(raw(:, [3,4,5,6]));


%% Make sure any text containing <undefined> is properly converted to an <undefined> categorical
for catIdx = [1,2,3]
    idx = (rawStringColumns(:, catIdx) == "<undefined>");
    rawStringColumns(idx, catIdx) = "";
end

%% Create output variable
participantdetails = table;
participantdetails.participantnum = cell2mat(rawNumericColumns(:, 1));
participantdetails.age = cell2mat(rawNumericColumns(:, 2));
participantdetails.gender = categorical(rawStringColumns(:, 1));
participantdetails.normalvision = categorical(rawStringColumns(:, 2));
participantdetails.bias = categorical(rawStringColumns(:, 3));
participantdetails.name = rawStringColumns(:, 4);
participantdetails.brightness = cell2mat(rawNumericColumns(:, 3));
participantdetails.ambient = cell2mat(rawNumericColumns(:, 4));
participantdetails.q1 = cell2mat(rawNumericColumns(:, 5));
participantdetails.q2 = cell2mat(rawNumericColumns(:, 6));
participantdetails.q3 = cell2mat(rawNumericColumns(:, 7));
participantdetails.q4 = cell2mat(rawNumericColumns(:, 8));
participantdetails.q5 = cell2mat(rawNumericColumns(:, 9));
participantdetails.q6 = cell2mat(rawNumericColumns(:, 10));


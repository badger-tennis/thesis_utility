function [emd, sim, roc, ccs, nss, kld, interemd, intersim, interroc, interccs, internss, interkld] = thirdExpKfoldResults(k, single_imdata, g_win_width, x_res, y_res, seconds)

% apparently when i wrote the fucntion this was based on I was too tired for loops or
% something, anyway it's pretty horrendous but it works, yay.

%g_win_width = 118;
%x_res = 1920;
%y_res = 1080;

%maps = cell(4,1);


fix_thresh = 34; % 34 calculated by hand based on Engelke, Maeder, Zepernick paper 
%"visual attention modelling for subjective image quality databases"

min_points = 4; % from same paper

%new_im = single_imdata;
%numP = length(single_imdata.indices);

% find where each exposure is

b1a1 = ((single_imdata.exposure_levels ==1) & (single_imdata.ambient_levels == 1));
ind_b1a1 = find(b1a1);
b1a2 = ((single_imdata.exposure_levels ==1) & (single_imdata.ambient_levels == 2));
ind_b1a2 = find(b1a2);
b1a3 = ((single_imdata.exposure_levels ==1) & (single_imdata.ambient_levels == 3));
ind_b1a3 = find(b1a3);
b2a1 = ((single_imdata.exposure_levels ==2) & (single_imdata.ambient_levels == 1));
ind_b2a1 = find(b2a1);
b2a2 = ((single_imdata.exposure_levels ==2) & (single_imdata.ambient_levels == 2));
ind_b2a2 = find(b2a2);
b2a3 = ((single_imdata.exposure_levels ==2) & (single_imdata.ambient_levels == 3));
ind_b2a3 = find(b2a3);

[emd1, sim1, roc1, ccs1, nss1, kld1, smooth1, kmaps1] = thirdExpOneKfold(k, single_imdata, ind_b1a1, min_points, fix_thresh, g_win_width, x_res, y_res, seconds);
[emd2, sim2, roc2, ccs2, nss2, kld2, smooth2, kmaps2] = thirdExpOneKfold(k, single_imdata, ind_b1a2, min_points, fix_thresh, g_win_width, x_res, y_res, seconds);
[emd3, sim3, roc3, ccs3, nss3, kld3, smooth3, kmaps3] = thirdExpOneKfold(k, single_imdata, ind_b1a3, min_points, fix_thresh, g_win_width, x_res, y_res, seconds);
[emd4, sim4, roc4, ccs4, nss4, kld4, smooth4, kmaps4] = thirdExpOneKfold(k, single_imdata, ind_b2a1, min_points, fix_thresh, g_win_width, x_res, y_res, seconds);
[emd5, sim5, roc5, ccs5, nss5, kld5, smooth5, kmaps5] = thirdExpOneKfold(k, single_imdata, ind_b2a2, min_points, fix_thresh, g_win_width, x_res, y_res, seconds);
[emd6, sim6, roc6, ccs6, nss6, kld6, smooth6, kmaps6] = thirdExpOneKfold(k, single_imdata, ind_b2a3, min_points, fix_thresh, g_win_width, x_res, y_res, seconds);

% [emd1, sim1, roc1, ccs1, nss1, kld1, smooth1, kmaps1] = thirdExpOneKfold(k, single_imdata, ind_1, min_points, fix_thresh, g_win_width, x_res, y_res, seconds);
% [emd2, sim2, roc2, ccs2, nss2, kld2, smooth2, kmaps2] = thirdExpOneKfold(k, single_imdata, ind_2, min_points, fix_thresh, g_win_width, x_res, y_res, seconds);
% [emd3, sim3, roc3, ccs3, nss3, kld3, smooth3, kmaps3] = thirdExpOneKfold(k, single_imdata, ind_3, min_points, fix_thresh, g_win_width, x_res, y_res, seconds);
% [emd4, sim4, roc4, ccs4, nss4, kld4, smooth4, kmaps4] = thirdExpOneKfold(k, single_imdata, ind_4, min_points, fix_thresh, g_win_width, x_res, y_res, seconds);

smooth = {smooth1, smooth2, smooth3; smooth4, smooth5, smooth6};
kmaps = {kmaps1, kmaps2, kmaps3; kmaps4 kmaps5, kmaps6};



intersize = size(kmaps{1});
interemd = cell(2, 2);
intersim = cell(2, 2);
interroc = cell(2, 2);
interccs = cell(2, 2);
internss = cell(2, 2);
interkld = cell(2, 2);

for t = 1:2
	for i = 2:3
		interemd{t, i-1} = zeros(intersize(1));
		intersim{t, i-1} = zeros(intersize(1));
		interroc{t, i-1} = zeros(intersize(1));
		interccs{t, i-1} = zeros(intersize(1));
		internss{t, i-1} = zeros(intersize(1));
		interkld{t, i-1} = zeros(intersize(1));
		for n = 1: intersize(1)
			for j = 1:intersize(1)
				interemd{t,i-1}(j, n) = EMD(smooth{t,1}{n, 2}, smooth{t,i}{j, 1}, 0, 64);
				intersim{t,i-1}(j, n) = similarity(smooth{t,1}{n, 2}, smooth{t,i}{j, 1}, 0);
				[interroc{t,i-1}(j, n), ~, ~, ~] = AUC_Judd(smooth{t,1}{n, 2}, kmaps{t,i}{j, 1}, 1, 0);
				interccs{t,i-1}(j, n) = CC(smooth{t,1}{n, 2}, smooth{t,i}{j, 1});
				internss{t,i-1}(j, n) = NSS(smooth{t,1}{n, 2}, kmaps{t,i}{j, 1});
				kla = KLdiv(smooth{t,1}{n, 2}, smooth{t,i}{j, 1});
				klb = KLdiv(smooth{t,i}{j, 1}, smooth{t,1}{n, 2});
				interkld{t,i-1}(j, n) = (kla+klb)/2;
			end
		end
	end
end

%res = [res1, res2, res3, res4];
emd = [emd1, emd2, emd3; emd4, emd5, emd6];
sim = [sim1, sim2, sim3; sim4, sim5, sim6];
roc = [roc1, roc2, roc3; roc4, roc5, roc6];
ccs = [ccs1, ccs2, ccs3; ccs4, ccs5, ccs6];
nss = [nss1, nss2, nss3; nss4, nss5, nss6];
kld = [kld1, kld2, kld3; kld4, kld5, kld6];
end

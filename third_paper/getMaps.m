function [maps] = getMaps(single_imdata, g_win_width, x_res, y_res, seconds)

maps = cell(2,3);
fix_thresh = 34; % 34 calculated by hand based on Engelke, Maeder, Zepernick paper 
%"visual attention modelling for subjective image quality databases"

min_points = 4; % from same paper

%new_im = single_imdata;
%numP = length(single_imdata.indices);

% find where each exposure is

% temp = (single_imdata.exposure_levels == 1);
% ind_1 = find(temp);
% temp = (single_imdata.exposure_levels == 2);
% ind_2 = find(temp);
% temp = (single_imdata.exposure_levels == 3);
% ind_3 = find(temp);
% temp = (single_imdata.exposure_levels == 4);
% ind_4 = find(temp);

b1a1 = ((single_imdata.exposure_levels ==1) & (single_imdata.ambient_levels == 1));
ind_b1a1 = find(b1a1);
b1a2 = ((single_imdata.exposure_levels ==1) & (single_imdata.ambient_levels == 2));
ind_b1a2 = find(b1a2);
b1a3 = ((single_imdata.exposure_levels ==1) & (single_imdata.ambient_levels == 3));
ind_b1a3 = find(b1a3);
b2a1 = ((single_imdata.exposure_levels ==2) & (single_imdata.ambient_levels == 1));
ind_b2a1 = find(b2a1);
b2a2 = ((single_imdata.exposure_levels ==2) & (single_imdata.ambient_levels == 2));
ind_b2a2 = find(b2a2);
b2a3 = ((single_imdata.exposure_levels ==2) & (single_imdata.ambient_levels == 3));
ind_b2a3 = find(b2a3);

maps{1,1} = oneMap(single_imdata, ind_b1a1, min_points, fix_thresh, g_win_width, x_res, y_res, seconds);
maps{1,2} = oneMap(single_imdata, ind_b1a2, min_points, fix_thresh, g_win_width, x_res, y_res, seconds);
maps{1,3} = oneMap(single_imdata, ind_b1a3, min_points, fix_thresh, g_win_width, x_res, y_res, seconds);
maps{2,1} = oneMap(single_imdata, ind_b2a1, min_points, fix_thresh, g_win_width, x_res, y_res, seconds);
maps{2,2} = oneMap(single_imdata, ind_b2a2, min_points, fix_thresh, g_win_width, x_res, y_res, seconds);
maps{2,3} = oneMap(single_imdata, ind_b2a3, min_points, fix_thresh, g_win_width, x_res, y_res, seconds);



end
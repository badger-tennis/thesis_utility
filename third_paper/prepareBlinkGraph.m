function [graphstuff] = prepareBlinkGraph(blink_info)

num = length(blink_info);
graphstuff = zeros(2,3);
counts = zeros(2,3);
for i = 1:num
    if ((blink_info(i, 1) == 500) && (blink_info(i,2) == 1))
        graphstuff(1,1) = graphstuff(1,1) + blink_info(i,3);
        counts(1,1) = counts(1,1) + 1;
    elseif ((blink_info(i, 1) == 500) && (blink_info(i,2) == 2))
        graphstuff(1,2) = graphstuff(1,2) + blink_info(i,3);
        counts(1,2) = counts(1,2) + 1;
    elseif ((blink_info(i, 1) == 500) && (blink_info(i,2) == 3))
        graphstuff(1,3) = graphstuff(1,3) + blink_info(i,3);
        counts(1,3) = counts(1,3) + 1;
    elseif ((blink_info(i, 1) == 8000) && (blink_info(i,2) == 1))
        graphstuff(2,1) = graphstuff(2,1) + blink_info(i,3);
        counts(2,1) = counts(2,1) + 1;
    elseif ((blink_info(i, 1) == 8000) && (blink_info(i,2) == 2))
        graphstuff(2,2) = graphstuff(2,2) + blink_info(i,3);
        counts(2,2) = counts(2,2) + 1;
    elseif ((blink_info(i, 1) == 8000) && (blink_info(i,2) == 3))
        graphstuff(2,3) = graphstuff(2,3) + blink_info(i,3);
        counts(2,3) = counts(2,3) + 1;
    else
        msg = 'fuck you';
        error(msg)
    end
end
graphstuff = graphstuff./counts;
end
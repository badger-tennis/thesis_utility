function [im_matrix] = gatherDataByImage(im_data, test_data)

        
numParticipants = length(test_data);
numImages = length(im_data);

for i = 1: numImages
   
	im_data{i}.gmt_ms = cell(numParticipants,1);
	
	im_data{i}.track_state = cell(numParticipants,1);
	
    im_data{i}.categoryLeft = cell(numParticipants, 1);
    im_data{i}.categoryRight = cell(numParticipants, 1);
    
    
    im_data{i}.fix_bool = cell(numParticipants,1);
	im_data{i}.blinking = cell(numParticipants,1);
	im_data{i}.saccade = cell(numParticipants,1);
    
	im_data{i}.gsi_pixel_x = cell(numParticipants,1);
	im_data{i}.gsi_pixel_y = cell(numParticipants,1);
	%im_data{i}.hpos_conf = cell(numParticipants,1);
    im_data{i}.pupil_diameter_l = cell(numParticipants,1);
    im_data{i}.pupil_diameter_r = cell(numParticipants,1);
    for j = 1: numParticipants
        
		%im_data{i}.gmt_s{j} = test_data{j}.gmt_s(im_data{i}.start_times(j):im_data{i}.end_times(j));
		im_data{i}.gmt_ms{j} = test_data{j}.RecordingTimems(im_data{i}.start_times(j):im_data{i}.end_times(j));
		
		im_data{i}.track_state{j} = test_data{j}.TrackingRatio(im_data{i}.start_times(j):im_data{i}.end_times(j));
		
        im_data{i}.categoryLeft{j} = test_data{j}.CategoryLeft(im_data{i}.start_times(j):im_data{i}.end_times(j));
        im_data{i}.categoryRight{j} = test_data{j}.CategoryRight(im_data{i}.start_times(j):im_data{i}.end_times(j));
        
        temp = ((test_data{j}.CategoryRight == 'Fixation') & (test_data{j}.CategoryLeft == 'Fixation'));
        im_data{i}.fix_bool{j} = temp(im_data{i}.start_times(j):im_data{i}.end_times(j));
        temp = ((test_data{j}.CategoryRight == 'Blink') & (test_data{j}.CategoryLeft == 'Blink'));
		im_data{i}.blinking{j} = temp(im_data{i}.start_times(j):im_data{i}.end_times(j));
        %im_data{i}.blinking{j} = test_data{j}.blinking(im_data{i}.start_times(j):im_data{i}.end_times(j));
		temp = ((test_data{j}.CategoryRight == 'Saccade') & (test_data{j}.CategoryLeft == 'Saccade'));
		im_data{i}.saccade{j}= temp(im_data{i}.start_times(j):im_data{i}.end_times(j));
        
        im_data{i}.gsi_pixel_x{j} = (test_data{j}.PointofRegardLeftXpx(im_data{i}.start_times(j):im_data{i}.end_times(j)) ...
            + test_data{j}.PointofRegardRightXpx(im_data{i}.start_times(j):im_data{i}.end_times(j)))/2;
		im_data{i}.gsi_pixel_y{j} = (test_data{j}.PointofRegardLeftYpx(im_data{i}.start_times(j):im_data{i}.end_times(j)) ...
            + test_data{j}.PointofRegardRightYpx(im_data{i}.start_times(j):im_data{i}.end_times(j)))/2;
        
		%im_data{i}.gsi_pixel_x{j} = test_data{j}.gsi_pixel_x(im_data{i}.start_times(j):im_data{i}.end_times(j));
		%im_data{i}.gsi_pixel_y{j} = test_data{j}.gsi_pixel_y(im_data{i}.start_times(j):im_data{i}.end_times(j));
		im_data{i}.pupil_diameter_l{j} = test_data{j}.PupilDiameterLeftmm(im_data{i}.start_times(j):im_data{i}.end_times(j));
        im_data{i}.pupil_diameter_r{j} = test_data{j}.PupilDiameterRightmm(im_data{i}.start_times(j):im_data{i}.end_times(j));
        
    end
end
im_matrix = im_data;
end
function [] = makeGraphData(metricString)

cd('D:\git_projects\thesis_utility\third_paper\raw_stats_files\CrossInt500Base');

raw_k2 = csvread(strcat('2_seconds_CrossInt4_', metricString, '.csv'));
raw_k12 = csvread(strcat('12_seconds_CrossInt4_', metricString, '.csv'));

k2 = mean(raw_k2);
k12 = mean(raw_k12);

newdata = [k2(1:3); k2(4:6); k12(1:3); k12(4:6)];

cd('D:\git_projects\Thesis\third_paper\graph_data');

csvwrite(strcat('cross_', metricString, '.csv'), newdata);

cd('D:\git_projects\thesis_utility\third_paper\raw_stats_files\CrossInt500Base');

end
k4kld = averageKfold(kkld);
csvwrite('2_seconds_kfold4_kld.csv', k4kld)
k4roc = averageKfold(kroc);
csvwrite('2_seconds_kfold4_roc.csv', k4roc)
k4emd = averageKfold(kemd);
csvwrite('2_seconds_kfold4_emd.csv', k4emd)
k4sim = averageKfold(ksim);
csvwrite('2_seconds_kfold4_sim.csv', k4sim)
k4nss = averageKfold(knss);
csvwrite('2_seconds_kfold4_nss.csv', k4nss)
k4ccs = averageKfold(kccs);
csvwrite('2_seconds_kfold4_cc.csv', k4ccs)
i4kld = averageInterResults(interkld, k4kld);
i4roc = averageInterResults(interroc, k4roc);
i4sim = averageInterResults(intersim, k4sim);
i4emd = averageInterResults(interemd, k4emd);
i4nss = averageInterResults(internss, k4nss);
i4ccs = averageInterResults(interccs, k4ccs);
csvwrite('2_seconds_int4_cc.csv', i4ccs)
csvwrite('2_seconds_int4_nss.csv', i4nss)
csvwrite('2_seconds_int4_kld.csv', i4kld)
csvwrite('2_seconds_int4_sim.csv', i4sim)
csvwrite('2_seconds_int4_emd.csv', i4emd)
csvwrite('2_seconds_int4_roc.csv', i4roc)
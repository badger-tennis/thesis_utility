function [result] = averageKfold(input)
result = zeros(60, 6);
for i = 1:60
    result(i, 1:3) = mean(input{i}(1:2, :));
    result(i, 4:6) = mean(input{i}(3:4, :));
end
end
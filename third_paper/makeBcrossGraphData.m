function [] = makeBcrossGraphData(metricString)

cd('D:\git_projects\thesis_utility\third_paper\raw_stats_files');

raw_k2 = csvread(strcat('bcross_2_second_', metricString, '.csv'));
raw_k12 = csvread(strcat('bcross_12_second_', metricString, '.csv'));

k2 = mean(raw_k2);
k12 = mean(raw_k12);

newdata = [k2; k12];

cd('D:\git_projects\Thesis\third_paper\graph_data');

csvwrite(strcat('bcross_', metricString, '.csv'), newdata);

cd('D:\git_projects\thesis_utility\third_paper\raw_stats_files');

end
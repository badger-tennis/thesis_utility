function [blink_info] = getBlinkInfoFull(data, sync_times, participantdetails)

blink_info = zeros(length(data),3);
for i = 1:length(data)
    blink_bool = ((data{i}.CategoryRight == 'Blink') & (data{i}.CategoryLeft == 'Blink'));
    blink_info(i,1) = participantdetails.brightness(i);
    blink_info(i,2) = participantdetails.ambient(i);
    blink_info(i,3) = getBlinkRateFull(blink_bool, sync_times{i});
end

end
function [kld_results, roc_results, emd_results, sim_results, cc_results, nss_results] = bintResults(fixMaps, fdms)

tic
emd_results = zeros(length(fdms),6);
for i = 1: length(fdms)
	emd_results(i, 1) = EMD(fdms{i}{1,1}, fdms{i}{1,2}, 0, 64);
	emd_results(i, 2) = EMD(fdms{i}{1,1}, fdms{i}{1,3}, 0, 64);
	emd_results(i, 3) = EMD(fdms{i}{1,2}, fdms{i}{1,3}, 0, 64);
	emd_results(i, 4) = EMD(fdms{i}{2,1}, fdms{i}{2,2}, 0, 64);
	emd_results(i, 5) = EMD(fdms{i}{2,1}, fdms{i}{2,3}, 0, 64);
	emd_results(i, 6) = EMD(fdms{i}{2,2}, fdms{i}{2,3}, 0, 64);
end
toc

fuckbeans = 0;
kld_results = zeros(length(fdms),6);
for i = 1: length(fdms)
	kl_tst = KLdiv(fdms{i}{1, 1}, fdms{i}{1, 2});
	kl_test2 = KLdiv(fdms{i}{1, 2}, fdms{i}{1, 1});
	if ~isreal(kl_tst)
		fuckbeans = fuckbeans + 1;
	end
	kld_results(i,1) = (kl_tst + kl_test2)/2;

	kl_tst = KLdiv(fdms{i}{1, 1}, fdms{i}{1, 3});
	kl_test2 = KLdiv(fdms{i}{1, 3}, fdms{i}{1, 1});
	if ~isreal(kl_tst)
		fuckbeans = fuckbeans + 1;
	end
	kld_results(i,2) = (kl_tst + kl_test2)/2;

	kl_tst = KLdiv(fdms{i}{1, 2}, fdms{i}{1, 3});
	kl_test2 = KLdiv(fdms{i}{1, 3}, fdms{i}{1, 2});
	if ~isreal(kl_tst)
		fuckbeans = fuckbeans + 1;
	end
	kld_results(i,3) = (kl_tst + kl_test2)/2;

	kl_tst = KLdiv(fdms{i}{2, 1}, fdms{i}{2, 2});
	kl_test2 = KLdiv(fdms{i}{2, 2}, fdms{i}{2, 1});
	if ~isreal(kl_tst)
		fuckbeans = fuckbeans + 1;
	end
	kld_results(i,4) = (kl_tst + kl_test2)/2;

	kl_tst = KLdiv(fdms{i}{2, 1}, fdms{i}{2, 3});
	kl_test2 = KLdiv(fdms{i}{2, 3}, fdms{i}{2, 1});
	if ~isreal(kl_tst)
		fuckbeans = fuckbeans + 1;
	end
	kld_results(i,5) = (kl_tst + kl_test2)/2;

	kl_tst = KLdiv(fdms{i}{2, 2}, fdms{i}{2, 3});
	kl_test2 = KLdiv(fdms{i}{2, 3}, fdms{i}{2, 2});
	if ~isreal(kl_tst)
		fuckbeans = fuckbeans + 1;
	end
	kld_results(i,6) = (kl_tst + kl_test2)/2;
end


sim_results = zeros(length(fdms),6);
for i = 1: length(fdms)
	%sim_results(i,j) = similarity(fdms{i}{1,j}, fdms{i}{2,j}, 0);
	sim_results(i, 1) = similarity(fdms{i}{1,1}, fdms{i}{1,2}, 0);
	sim_results(i, 2) = similarity(fdms{i}{1,1}, fdms{i}{1,3}, 0);
	sim_results(i, 3) = similarity(fdms{i}{1,2}, fdms{i}{1,3}, 0);
	sim_results(i, 4) = similarity(fdms{i}{2,1}, fdms{i}{2,2}, 0);
	sim_results(i, 5) = similarity(fdms{i}{2,1}, fdms{i}{2,3}, 0);
	sim_results(i, 6) = similarity(fdms{i}{2,2}, fdms{i}{2,3}, 0);
end

roc_results = zeros(length(fdms),6);
for i = 1: length(fdms)
	[roc_results(i,1), ~,~,~] = AUC_Judd(fdms{i}{1,1}, fixMaps{i}{1,2}, 1, 0);
	[roc_results(i,2), ~,~,~] = AUC_Judd(fdms{i}{1,1}, fixMaps{i}{1,3}, 1, 0);
	[roc_results(i,3), ~,~,~] = AUC_Judd(fdms{i}{1,2}, fixMaps{i}{1,3}, 1, 0);
	[roc_results(i,4), ~,~,~] = AUC_Judd(fdms{i}{2,1}, fixMaps{i}{2,2}, 1, 0);
	[roc_results(i,5), ~,~,~] = AUC_Judd(fdms{i}{2,1}, fixMaps{i}{2,3}, 1, 0);
	[roc_results(i,6), ~,~,~] = AUC_Judd(fdms{i}{2,2}, fixMaps{i}{2,3}, 1, 0);
end

cc_results = zeros(length(fdms),6);
for i = 1: length(fdms)
	%cc_results(i,j) = CC(fdms{i}{1,j}, fdms{i}{2,j}); 
	cc_results(i, 1) = CC(fdms{i}{1,1}, fdms{i}{1,2});
	cc_results(i, 2) = CC(fdms{i}{1,1}, fdms{i}{1,3});
	cc_results(i, 3) = CC(fdms{i}{1,2}, fdms{i}{1,3});
	cc_results(i, 4) = CC(fdms{i}{2,1}, fdms{i}{2,2});
	cc_results(i, 5) = CC(fdms{i}{2,1}, fdms{i}{2,3});
	cc_results(i, 6) = CC(fdms{i}{2,2}, fdms{i}{2,3});
end

nss_results = zeros(length(fdms),6);
for i = 1: length(fdms)
	nss_results(i,1) = NSS(fdms{i}{1,1}, fixMaps{i}{2,2});
	nss_results(i,2) = NSS(fdms{i}{1,1}, fixMaps{i}{2,3});
	nss_results(i,3) = NSS(fdms{i}{1,2}, fixMaps{i}{2,3});
	nss_results(i,4) = NSS(fdms{i}{2,1}, fixMaps{i}{2,2});
	nss_results(i,5) = NSS(fdms{i}{2,1}, fixMaps{i}{2,3});
	nss_results(i,6) = NSS(fdms{i}{2,2}, fixMaps{i}{2,3});
end

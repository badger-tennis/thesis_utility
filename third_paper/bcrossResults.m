function [kld_results, roc_results, emd_results, sim_results, cc_results, nss_results] = bcrossResults(fixMaps, fdms)
fuckbeans = 0;
kld_results = zeros(length(fdms),3);
for i = 1: length(fdms)
	for j = 1:3
		kl_tst = KLdiv(fdms{i}{1, j}, fdms{i}{2, j});
		kl_test2 = KLdiv(fdms{i}{2, j}, fdms{i}{1, j});
		if ~isreal(kl_tst)
			fuckbeans = fuckbeans + 1;
		end
		kld_results(i,j) = (kl_tst + kl_test2)/2;
	end
end

tic
emd_results = zeros(length(fdms), 3);
for i = 1: length(fdms)
	for j = 1:3
		emd_results(i, j) = EMD(fdms{i}{1,j}, fdms{i}{2,j}, 0, 64);
	end
end
toc

sim_results = zeros(length(fdms),3);
for i = 1: length(fdms)
	for j = 1:3
		sim_results(i,j) = similarity(fdms{i}{1,j}, fdms{i}{2,j}, 0);
	end
end

roc_results = zeros(length(fdms),3);
for i = 1: length(fdms)
	for j = 1:3
		[roc_results(i,j), ~,~,~] = AUC_Judd(fdms{i}{1,j}, fixMaps{i}{2,j}, 1, 0);
	end
end

cc_results = zeros(length(fdms),3);
for i = 1: length(fdms)
	for j = 1:3
		cc_results(i,j) = CC(fdms{i}{1,j}, fdms{i}{2,j}); 
	end
end

nss_results = zeros(length(fdms),3);
for i = 1: length(fdms)
	for j = 1:3
		nss_results(i,j) = NSS(fdms{i}{1,j}, fixMaps{i}{2,j});
	end
end

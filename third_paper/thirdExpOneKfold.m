function [emd, sim, roc, ccs, nss, kld, smooth, kmaps] = thirdExpOneKfold(k, single_imdata, indices, min_points, fix_thresh, g_win_width, x_res, y_res, seconds)

time_ms = seconds*1000;
numImages = length(indices);
if ~(numImages == 8 || numImages == 7)
    msg = 'giant fuckup in single map';
    error(msg)
end
maps1 = cell(numImages,1);
for i = 1: numImages
    if seconds ~= 0
        refTime = single_imdata.gmt_ms{indices(i)}(1);
        interval = find(single_imdata.gmt_ms{indices(i)}>(refTime + time_ms),1);
        %if interval < 60
         %   interval;
        %end
        %blinking = single_imdata.blinking{indices(i)}(1:interval);
        %saccade = single_imdata.saccade{indices(i)}(1:interval);
        fix_bool = single_imdata.fix_bool{indices(i)}(1:interval);
        gsi_pixel_x = single_imdata.gsi_pixel_x{indices(i)}(1:interval);
        gsi_pixel_y = single_imdata.gsi_pixel_y{indices(i)}(1:interval);
    else
        %blinking = single_imdata.blinking{indices(i)};
        %saccade = single_imdata.saccade{indices(i)};
        fix_bool = single_imdata.fix_bool{indices(i)};
        gsi_pixel_x = single_imdata.gsi_pixel_x{indices(i)};
        gsi_pixel_y = single_imdata.gsi_pixel_y{indices(i)};
    end
    % Here beginneth risky logic shite
    %log_check = ((blinking == 0) & (saccade == 0) & (fix_bool == 1));
    
    %log_check = ((single_imdata.blinking{indices(i)} == 0) & (single_imdata.saccade{indices(i)} == 0) & (single_imdata.gaze_scr_intsect{indices(i)} == 1));
        
    pixel_x = gsi_pixel_x(fix_bool);
    
    %pixel_x = single_imdata.gsi_pixel_x{indices(i)}(log_check);
    
    pixel_y = gsi_pixel_y(fix_bool);
    
    intersect = (pixel_x > 0 & pixel_y >0 & pixel_x < 1920 & pixel_y < 1080);
    
    pixel_x = pixel_x(intersect);
    pixel_y = pixel_y(intersect);
    %pixel_y = single_imdata.gsi_pixel_y{indices(i)}(log_check);
    
    maps1{i} = fdm(pixel_x, pixel_y, x_res, y_res, g_win_width, fix_thresh, min_points);
end


kmaps = kfoldMapsThird(k, numImages, maps1, x_res,y_res);
ksize = size(kmaps);

smooth = cell(ksize);
for i = 1: ksize(1)
    fd_t = run_antonioGaussian(kmaps{i,1}, (g_win_width/2));
    fd_t(fd_t<0) = 0;
    smooth{i,1} = fd_t;
    
    fd_t = run_antonioGaussian(kmaps{i,2}, (g_win_width/2));
    fd_t(fd_t<0) = 0;
    smooth{i,2} = fd_t;
end

%kmaps column one is inner (validation) group
%second column is larger 'training' group
emd = zeros(ksize(1), 1);
sim = zeros(ksize(1), 1);
roc = zeros(ksize(1), 1);
ccs = zeros(ksize(1), 1);
nss = zeros(ksize(1), 1);
kld = zeros(ksize(1), 1);
for i = 1: ksize(1)
    emd(i) = EMD(smooth{i, 2}, smooth{i, 1}, 0, 64);
    sim(i) = similarity(smooth{i, 2}, smooth{i, 1}, 0);
    [roc(i), ~,~,~] = AUC_Judd(smooth{i,2}, kmaps{i,1}, 1, 0);
    ccs(i) = CC(smooth{i,2}, smooth{i,1});
    nss(i) = NSS(smooth{i,2}, kmaps{i,1});
    kla = KLdiv(smooth{i,2}, smooth{i,1});
    klb = KLdiv(smooth{i,1}, smooth{i,2});
    kld(i) = (kla+klb)/2;
end



%     for i = 1: length(im_data)
%         for j = 1:4
%             emd_results(i, j) = EMD(fdms{i, j}, fdms{i, 1}, 0, 64);
%         end
%     end
% end
% toc
% 
% if ~(ismember('sim_results', who))
%     sim_results = zeros(length(im_data),4);
%     for i = 1: length(im_data)
%         for j = 1:4
%             sim_results(i,j) = similarity(fdms{i, j}, fdms{i, 1}, 0);
%         end
%     end
% end
% 
% if ~(ismember('roc_results', who))
%     roc_results = zeros(length(im_data),4);
%     for i = 1: length(im_data)
%         for j = 1:4
%             [roc_results(i,j), ~,~,~] = AUC_Judd(fdms{i, j}, fixMaps{i}{1}, 1, 0); % This is wrong
%         end
%     end
% end


end
function [res] = visualiseFixMap(fixMap, colour)

[row,col] = find(fixMap == 1);
rads = ones(length(col),1)*20;

blank = zeros(size(fixMap));
res = insertShape(blank, 'FilledCircle', [col row rads], 'Color', colour);
end
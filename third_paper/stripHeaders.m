% This was for stripping the headers from the raw output of the iViewX
% package, so results could be processed. I'd say don't run it but I've
% hard-coded some paths so unless you're me or use my exact file structure
% that should be impossible

destin = "D:/Google Drive/third_results/";

list = dir('*.txt');
num = length(list);
form = '%s';

for i = 1:num
    temp = strrep(list(i).name,'res - ','');
    newName = strcat(destin,strrep(temp,'.txt','.csv'));
    fid1 = fopen(list(i).name, 'r');
    fid2 = fopen(newName, 'w');
    for j = 1:5
        tline = fgets(fid1);
    end
    while ischar(tline)
        fprintf(fid2, form, tline);
        tline = fgets(fid1);
    end
    fclose(fid1);
    fclose(fid2);
end
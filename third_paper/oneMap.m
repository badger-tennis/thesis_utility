 function [map] = oneMap(single_imdata, indices, min_points, fix_thresh, g_win_width, x_res, y_res, seconds)

%if seconds ~= 0
%    interval = 60*seconds;
%end

time_ms = seconds*1000;
numImages = length(indices);
if ~(numImages == 8 || numImages == 7)
    msg = 'giant fuckup in single map';
    error(msg)
end
maps1 = cell(numImages,1);
for i = 1: numImages
    if seconds ~= 0
        refTime = single_imdata.gmt_ms{indices(i)}(1);
        interval = find(single_imdata.gmt_ms{indices(i)}>(refTime + time_ms),1);
        %if interval < 60
         %   interval;
        %end
        %blinking = single_imdata.blinking{indices(i)}(1:interval);
        %saccade = single_imdata.saccade{indices(i)}(1:interval);
        fix_bool = single_imdata.fix_bool{indices(i)}(1:interval);
        gsi_pixel_x = single_imdata.gsi_pixel_x{indices(i)}(1:interval);
        gsi_pixel_y = single_imdata.gsi_pixel_y{indices(i)}(1:interval);
    else
        %blinking = single_imdata.blinking{indices(i)};
        %saccade = single_imdata.saccade{indices(i)};
        fix_bool = single_imdata.fix_bool{indices(i)};
        gsi_pixel_x = single_imdata.gsi_pixel_x{indices(i)};
        gsi_pixel_y = single_imdata.gsi_pixel_y{indices(i)};
    end
    % Here beginneth risky logic shite
    %log_check = ((blinking == 0) & (saccade == 0) & (fix_bool == 1));
    
    %log_check = ((single_imdata.blinking{indices(i)} == 0) & (single_imdata.saccade{indices(i)} == 0) & (single_imdata.gaze_scr_intsect{indices(i)} == 1));
        
    pixel_x = gsi_pixel_x(fix_bool);
    
    %pixel_x = single_imdata.gsi_pixel_x{indices(i)}(log_check);
    
    pixel_y = gsi_pixel_y(fix_bool);
    
    intersect = (pixel_x > 0 & pixel_y >0 & pixel_x < 1920 & pixel_y < 1080);
    
    pixel_x = pixel_x(intersect);
    pixel_y = pixel_y(intersect);
    %pixel_y = single_imdata.gsi_pixel_y{indices(i)}(log_check);
    
    maps1{i} = fdm(pixel_x, pixel_y, x_res, y_res, g_win_width, fix_thresh, min_points);
end

final = zeros(y_res, x_res);
for i = 1: numImages
    final = final + maps1{i};
end
%fin1 = fin1./num1s;

    %sigma = g_win_width/2;
    %filt = fspecial('gaussian', g_win_width, sigma);
    %temp_map = imfilter(fin1, filt);
    %peak = max(max(temp_map));
    %maps{1} = temp_map./peak;
    
peak = max(max(final));
final(final>1) = 1;
map = final;
%maps{1} = fin1./peak;

end
function [blink_info] = getBlinkInfo(data, sync_times, participantdetails)

%blink info, cell array, 3 long, 1 amb level in each. hmm, sort by
%brightness too? yes. Ok 2 by 3, same as fixation maps.
blink_info = cell(6,1);
inds = cell(6,1);

%this is a bit frankenstein-esque but it's more foolproof and I don't have
%the time to be elegant atm
b1a1 = ((participantdetails.brightness == 500) & (participantdetails.ambient == 1));
ind_b1a1 = find(b1a1);
inds{1} = ind_b1a1;
b1a2 = ((participantdetails.brightness == 500) & (participantdetails.ambient == 2));
ind_b1a2 = find(b1a2);
inds{2} = ind_b1a2;
b1a3 = ((participantdetails.brightness == 500) & (participantdetails.ambient == 3));
ind_b1a3 = find(b1a3);
inds{3} = ind_b1a3;
b2a1 = ((participantdetails.brightness == 8000) & (participantdetails.ambient == 1));
ind_b2a1 = find(b2a1);
inds{4} = ind_b2a1;
b2a2 = ((participantdetails.brightness == 8000) & (participantdetails.ambient == 2));
ind_b2a2 = find(b2a2);
inds{5} = ind_b2a2;
b2a3 = ((participantdetails.brightness == 8000) & (participantdetails.ambient == 3));
ind_b2a3 = find(b2a3);
inds{6} = ind_b2a3;

for j = 1:length(inds)
    blink_info{j} = zeros(length(inds{j}), 4);
    for i = 1:length(inds{j})
        blink_bool = ((data{inds{j}(i)}.CategoryRight == 'Blink') & (data{inds{j}(i)}.CategoryLeft == 'Blink'));
        blink_info{j}(i,:) = getBlinkRate(blink_bool, sync_times{inds{j}(i)});
    end
end

end
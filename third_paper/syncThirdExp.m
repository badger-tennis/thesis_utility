% sync

%find last enter except if over 20000 (single edge case from capture)

%advance three seconds

%get local time

%find differential

function [time_diffs, start_times, sync_times] = syncThirdExp(data, eye_logs, numParticipants, numImages)
time_diffs = zeros(numParticipants,1);
start_times = zeros(numParticipants, 1);
sync_times = cell(numParticipants, 1);

for i = 1:numParticipants
    sync_times{i} = zeros(numImages, 2);
    if i == 23
        x = 2;
    end
    temp = find(data{i}.Content == 'Return');
    inds = temp(temp<20000);
    lastind = length(inds); 
    keypressTime = data{i}.RecordingTimems(inds(lastind));
    start = find(data{i}.RecordingTimems>(keypressTime + 3000),1);
    %start = inds(lastind) + 180; % + 180 moves past the 3s spacer at the start
    time = data{i}.RecordingTimems(start);
    time_diffs(i) = eye_logs{i,2}(1) - time;
    
    for j = 1: numImages
        sync_times{i}(j,1) = find(data{i}.RecordingTimems>(eye_logs{i,2}(j) - time_diffs(i)),1);
        sync_times{i}(j,2) = find(data{i}.RecordingTimems>(eye_logs{i,3}(j) - time_diffs(i)),1);
        %if (sync_times{i}(j,2) - sync_times{i}(j,1))<600
         %   imSoftwareTime = eye_logs{i,3}(j) - eye_logs{i,2}(j);
         %   eyeSoftwareTime = data{i}.RecordingTimems(sync_times{i}(j,2)) - data{i}.RecordingTimems(sync_times{i}(j,1));
        %end
    end
end



end

% function [sync_times, retparticipants] = sync_images(data, eye_logs, participants)
% 
% x = size(eye_logs);
% numP = x(1);
% numI = length(eye_logs{1,1});
% sync_times = cell(numP,1);
% 
% for i = 1: numP
%     sync_times{i} = zeros(numI, 2);
%     data_entries = length(data{i}.gmt_s);
%     participants{i}.ptime = (data{i}.gmt_s.*1000) + data{i}.gmt_ms;
%     for j = 1: numI
%         % debug test
%         %if ((i == 14) && (j == 80))
%          %   x = 4;
%         %end
%         
%         sync_times{i}(j,1) = (find((participants{i}.ptime)>(eye_logs{i,2}(j)),1)) - 1;
%         if (participants{i}.ptime(data_entries) ==  eye_logs{i,3}(j))
%             sync_times{i}(j,2) = data_entries;
%         else
%             sync_times{i}(j,2) = (find((participants{i}.ptime)>(eye_logs{i,3}(j)),1)) - 1;
%         end
%     end
%     participants{i}.sync_times = sync_times{i};
%     participants{i}.image_order = get_image_names(eye_logs{i,1}, 0);
% end
% retparticipants = participants;
% % ind = 3;
% % % eye_log{1} = eye_logs{ind, 1};
% % % eye_log{2} = eye_logs{ind, 2};
% % eye_log{3} = eye_logs{ind, 3};
% % 
% % test_data = data{ind};
% 
% ret = cell(size(eye_log));
% 
% data_entries = length(test_data.gmt_s);
% log_entries = length(eye_log{1});
% ptime = zeros(data_entries,1);
% 
% for i = 1: data_entries
%     ptime(i) = (test_data.gmt_s(i)*1000 + test_data.gmt_ms(i));
% end
% 
% count = 1;
% while ptime(count) < eye_log{2}(1)
%     count = count + 1;
% end
% 
% over_diff = ptime(count) - eye_log{2}(1);
% under_diff = eye_log{2}(1) - ptime(count-1);
% 
% under = 0;
% if under_diff < over_diff
%     count = count -1;
%     under = 1;
%     diff = eye_log{2}(1) - ptime(count);
% else 
%     diff = ptime(count) - eye_log{2}(1);
% end
% 
% sync_start = count;
% % a = ptime(count)
% % b = eye_log{2}(1)
% % c = diff

%load output of experiment program

%paths: home: function locations, results_dir: eyetracker output, logpath: image software output, p_details: demographic data csv
%Home paths
home = 'D:\git_projects\thesis_utility\third_paper';
results_dir = 'D:\Google Drive\third_results\';
logpath = 'D:\git_projects\Thesis\third_paper\logs\';
p_details = 'D:\git_projects\Thesis\third_paper\results\participant_details.csv';

% Work paths
% home = 'C:\Users\bradle_t\Development\thesis_utility\third_paper';
% results_dir = 'C:\Users\bradle_t\Google Drive\third_results\';
% logpath = 'C:\Users\bradle_t\Development\thesis\third_paper\logs';
% p_details = 'C:\Users\bradle_t\Development\thesis\third_paper\results\participant_details.csv';

%test_dir = 'D:\git_projects\for_test_stuff\stripHeadersTest\';

% get data (image shown, time shown start, time shown end) from HDR image
% display software logs
formatspec = '%s %u64 %u64';
if ~(ismember('eye_logs', who))
    eye_logs = load_eye_logs(logpath, formatspec);
end
cd(home);

numParticipants = length(eye_logs);

% get data from CSVs output by the eyetracker software
if ~(ismember('data', who))
    %[data, names] = read_eye_tracker_data(results_dir, numParticipants);
    cd(results_dir);
    load('rawresults.mat');
    cd(home)
    data(23) = []; % data was trash, eye tracker stopped tracking after 6 mins
end



if ~(ismember('participantdetails', who))
    participantdetails = importParticipantInfo(p_details);
    participantdetails(23,:) = [];
end

low500 = find((participantdetails.ambient == 1) & (participantdetails.brightness == 500));
mid500 = find((participantdetails.ambient == 2) & (participantdetails.brightness == 500));
high500 = find((participantdetails.ambient == 3) & (participantdetails.brightness == 500));

low8000 = find((participantdetails.ambient == 1) & (participantdetails.brightness == 8000));
mid8000 = find((participantdetails.ambient == 2) & (participantdetails.brightness == 8000));
high8000 = find((participantdetails.ambient == 3) & (participantdetails.brightness == 8000));

imageNames = get_image_names(eye_logs{1,1}, 1);
numImages = length(imageNames);
im_data = initialise_im_data(eye_logs, imageNames);

[time_diffs, start_times, sync_times] = syncThirdExp(data, eye_logs, numParticipants, numImages);

im_data = get_start_end(im_data, sync_times);

im_data = get_exposure_levels2(im_data, eye_logs, participantdetails);

im_data = gatherDataByImage(im_data, data);

if ~(ismember('blink_info', who))
      
    blink_info = getBlinkInfoFull(data, sync_times, participantdetails);
end
clear data

gauss_window = 118;
seconds = 0;
x_res = 1920;
y_res = 1080;
% if ~(ismember('fixMaps', who))
%     fixMaps = cell(length(im_data),1);
%     for i = 1: length(im_data)        
%         fixMaps{i} = getMaps(im_data{i}, gauss_window, x_res, y_res, seconds); %seconds is a cutoff 0 is default
%     end
% end
% 
% if ~(ismember('fdms', who))
%     fdms = cell(length(im_data),1);
%     for i = 1: length(im_data)
%         fdms{i} = cell(2,3);
%         for j = 1: 2
%             for k = 1:3
%                 [fd_t, ~] = run_antonioGaussian(fixMaps{i}{j,k}, (gauss_window/2));
%                 fd_t(fd_t<0) = 0;
%                 fdms{i}{j,k} = fd_t;
%             end
%         end
%     end
% end



k =4;
tic
% not sure whether brightness one needs to be included in inter-comparisons
if ~(ismember('k_res', who))
    k_res = 1;
    kemd = cell(length(im_data),1);
    ksim = cell(length(im_data),1);
    kroc = cell(length(im_data),1);
    kccs = cell(length(im_data),1);
    knss = cell(length(im_data),1);
    kkld = cell(length(im_data),1);
    interemd = cell(length(im_data),1);
    intersim = cell(length(im_data),1);
    interroc = cell(length(im_data),1);
    interccs = cell(length(im_data),1);
    internss = cell(length(im_data),1);
    interkld = cell(length(im_data),1);
    for i = 1: length(im_data)
         [kemd{i}, ksim{i}, kroc{i}, kccs{i}, knss{i}, kkld{i}, interemd{i}, intersim{i}, interroc{i}, interccs{i}, internss{i}, interkld{i}] ...
    = thirdExpKfoldResultsExtra(k, im_data{i}, gauss_window, x_res, y_res, seconds);
    end
end
toc

%[kld_results, roc_results, emd_results, sim_results, cc_results, nss_results] = bintResults(fixMaps, fdms);
%[kld_results, roc_results, emd_results, sim_results, cc_results, nss_results] = bcrossResults(fixMaps, fdms);
% 500v81 5v82 5v83 51v52 52v53 51v53
% 

% 
% if ~(ismember('results', who))
%     [results, means, stddv] = process_perclos(participants);
% end








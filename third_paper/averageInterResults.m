function [result] = averageInterResults(input, kinput)

result = zeros(60, 6);
for i = 1: 60
    result(i, 1) = kinput(i, 1);
    result(i, 2) = mean(mean(input{i}{1,2}));
    result(i, 3) = mean(mean(input{i}{1,3}));
    result(i, 4) = mean(mean(input{i}{2,1}));
    result(i, 5) = mean(mean(input{i}{2,2}));
    result(i, 6) = mean(mean(input{i}{2,3}));
end

end